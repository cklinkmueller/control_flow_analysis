library(lattice)

table <- read.csv("D:/workspaces/submission/control_flow/src/test/resources/birth/ors_random_alignments.csv", dec = ".", header = TRUE, sep = ",", stringsAsFactors=FALSE)
#table <- read.csv("D:/workspaces/submission/control_flow/src/test/resources/uni/ors_random_alignments.csv", dec = ".", header = TRUE, sep = ",", stringsAsFactors=FALSE)

d <- data.frame(F=table$fmic,S=table$orssta,E=table$orsend,D=table$orsdep,B=table$orsstrict)
#d <- data.frame(F=table$fmic,S=table$orssta,E=table$orsend,D=table$orsdep)

cor(d,method="spearman")
cor.test(d$F,d$S,method="spearman")
cor.test(d$F,d$E,method="spearman")
cor.test(d$F,d$D,method="spearman")
cor.test(d$F,d$B,method="spearman")
cor.test(d$S,d$E,method="spearman")
cor.test(d$S,d$D,method="spearman")
cor.test(d$S,d$B,method="spearman")
cor.test(d$E,d$D,method="spearman")
cor.test(d$E,d$B,method="spearman")
cor.test(d$D,d$B,method="spearman")

pairs(~F+S+E+D+B,data=d, main="Simple Scatterplot Matrix")


#cor(d,method="spearman")
#cor.test(d$F,d$S,method="spearman")
#cor.test(d$F,d$E,method="spearman")
#cor.test(d$F,d$D,method="spearman")
#cor.test(d$S,d$E,method="spearman")
#cor.test(d$S,d$D,method="spearman")
#cor.test(d$E,d$D,method="spearman")

#pairs(~F+S+E+D,data=d, main="Simple Scatterplot Matrix")
