
package org.tartarus.snowball;
import java.lang.reflect.InvocationTargetException;

// Copyright (c) 2001, Dr Martin Porter,
// Copyright (c) 2002, Richard Boulton.
// All rights reserved.

public abstract class SnowballStemmer extends SnowballProgram {
    public abstract boolean stem();
};
