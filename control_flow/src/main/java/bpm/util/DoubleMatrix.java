package bpm.util;

import java.util.HashMap;
import java.util.Map;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class DoubleMatrix<K> {
	private Map<K, Integer> rows;
	private Map<K, Integer> columns;
	private double[][] values;
	
	public DoubleMatrix(Iterable<K> rows, Iterable<K> columns, double initialValue) {
		this.rows = new HashMap<K, Integer>();
		this.columns = new HashMap<K, Integer>();
		
		for (K r : rows) {
			this.rows.put(r, this.rows.size());
		}
		
		for (K c : columns) {
			this.columns.put(c, this.columns.size());
		}
		
		this.values = new double[this.rows.size()][this.columns.size()];
		for (K r : this.rows.keySet()) {
			int i1 = this.rows.get(r);
			for (K c : this.columns.keySet()) {
				int i2 = this.columns.get(c);
				this.values[i1][i2] = initialValue;
			}
		}
	}
	
	public void setValue(K row, K column, double value) {
		this.values[this.rows.get(row)][this.columns.get(column)] = value;
	}
	
	public double value(K row, K column) {
		return this.values[this.rows.get(row)][this.columns.get(column)];
	}

	public boolean containsColumn(String word) {
		return this.columns.containsKey(word);
	}

	public boolean containsRow(String word) {
		return this.rows.containsKey(word);
	}
}
