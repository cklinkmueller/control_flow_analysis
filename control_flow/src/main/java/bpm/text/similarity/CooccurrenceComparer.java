package bpm.text.similarity;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

import bpm.text.cooc.Cooccurrence;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class CooccurrenceComparer extends WordComparer {
	private Cooccurrence cooccurrence;
	private int k;
	
	public CooccurrenceComparer(Cooccurrence cooccurrence) {
		this(cooccurrence, Integer.MAX_VALUE);
	}
	
	public CooccurrenceComparer(Cooccurrence cooccurrence, int k) {
		this.cooccurrence = cooccurrence;
		this.k = k;		
	}
	
	public void setK(int k) {
		this.k = k;
	}

	@Override
	protected double determineSimilarity(String word1, String word2) {
		LinkedList<String> kwords1 = this.cooccurrence.getTopKindex(word1, word2, this.k);
		LinkedList<String> kwords2 = this.cooccurrence.getTopKindex(word2, word1, this.k);
		
		HashSet<String> set = new HashSet<String>();
		set.addAll(kwords1);
		set.addAll(kwords2);
		
		return this.getSimilarity(set, word1, word2);		
	}

	public double getSimilarity(Collection<String> words, String word1, String word2) {
		double vw = 0;
		double v = 0;
		double w = 0;
		
		for (String k : words) {
			double v1 = this.cooccurrence.getCooccurrence(word1, k);
			double v2 = this.cooccurrence.getCooccurrence(word2, k);
				
			vw += v1 * v2;
			v += v1 * v1;
			w += v2 * v2;
		}
		
		double sim = (v == 0 || w == 0) ? 0 : vw / Math.sqrt(v * w);		
		return sim;
	}	
	
}
