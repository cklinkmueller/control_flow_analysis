package bpm.text.similarity;

import java.util.List;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

abstract class StemComparer extends WordComparer {
	private WordComparer comparer;
		
	public StemComparer() {
		this(new LevenshteinComparer());
	}
	
	public StemComparer(WordComparer comparer) {
		this.comparer = comparer;
	}
	
	@Override
	protected double determineSimilarity(String word1, String word2) {
		List<String> stems1 = this.getStems(word1);
		List<String> stems2 = this.getStems(word2);
		
		double max = 0;
		for (String w1 : stems1) {
			for (String w2 : stems2) {
				double sim = this.comparer.getSimilarity(w1, w2);
				max = max > sim ? max : sim;
			}
		}
		
		return max;
	}

	protected List<String> getStems(String word) {
		List<String> stems = this.stem(word);
		stems.add(word);
		return stems;
	}
	
	protected abstract List<String> stem(String word);
	
}
