package bpm.text.similarity;

import edu.cmu.lti.ws4j.WS4J;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class LinWordNetComparer extends WordComparer {
	@Override
	protected double determineSimilarity(String word1, String word2) {
		double sim = WS4J.runLIN(word1, word2);
		return sim > 1.0 ? 1.0 : Math.abs(sim);
	}
	
}