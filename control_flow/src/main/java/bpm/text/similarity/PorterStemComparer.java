package bpm.text.similarity;

import java.util.LinkedList;
import java.util.List;

import bpm.text.TextUtils;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class PorterStemComparer extends StemComparer {
	
	public PorterStemComparer() {
		this(new LevenshteinComparer());
	}
	
	public PorterStemComparer(WordComparer comparer) {
		super(comparer);
	}

	@Override
	protected List<String> stem(String word) {
		LinkedList<String> stem = new LinkedList<String>();
		stem.add(TextUtils.stemPorter(word));
		return stem;
	}	

}
