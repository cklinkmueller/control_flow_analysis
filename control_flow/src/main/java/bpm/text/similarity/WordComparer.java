package bpm.text.similarity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bpm.model.Node;
import bpm.model.ProcessUtilities;
import bpm.model.Process;
import bpm.text.TextUtils;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public abstract class WordComparer {
	private double[][] similarities;
	private HashMap<String, Integer> indecies;
	
	public WordComparer() {
		this.indecies = new HashMap<String, Integer>();
	}
	
	public void initialize(List<Process> processes) {
		this.extractIndecies(processes);
		
		this.determineSimilarities();
	}
	
	private void determineSimilarities() {
		ArrayList<String> words = new ArrayList<String>(this.indecies.keySet());
		this.similarities = new double[words.size()][words.size()];
				
		for (int a = 0; a < words.size(); a++) {
			String word1 = words.get(a);
			int i1 = this.indecies.get(word1);
			this.similarities[i1][i1] = this.determineSimilarity(word1, word1);
			
			for (int b = a + 1; b < words.size(); b++) {
				String word2 = words.get(b);
				int i2 = this.indecies.get(word2);
				double sim = this.determineSimilarity(word1, word2);
				
				this.similarities[i1][i2] = this.similarities[i2][i1] = sim;
			}
		}		
	}

	private void extractIndecies(List<Process> processes) {
		this.indecies = new HashMap<String, Integer>();
		
		for (Process process : processes) {
			for (Node act : ProcessUtilities.getActivities(process)) {
				for (String word : TextUtils.tokenizeAndRemoveStopWords(act.getLabel())) {
					if (!this.indecies.containsKey(word)) {
						this.indecies.put(word, this.indecies.size());
					}
				}
			}
		}
	}

	public final double getSimilarity(String word1, String word2) {
		Integer i1 = this.indecies.get(word1);
		Integer i2 = this.indecies.get(word2);
		
		if (i1 == null || i2 == null) {
			return this.determineSimilarity(word1, word2);
		}
		
		return this.similarities[i1][i2];
	}

	protected abstract double determineSimilarity(String word1, String word2);
}
