package bpm.text.similarity;

import org.apache.commons.lang3.StringUtils;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class LevenshteinComparer extends WordComparer {
	@Override
	protected double determineSimilarity(String word1, String word2) {
		double distance = StringUtils.getLevenshteinDistance(word1, word2);
		distance /= word1.length() > word2.length() ? word1.length() : word2.length();		
		return 1 - distance;
	}
	
}
