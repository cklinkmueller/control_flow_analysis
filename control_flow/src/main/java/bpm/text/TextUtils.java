package bpm.text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import bpm.text.stem.EnglishStemmer;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class TextUtils {
	private static final HashSet<String> STOP_WORDS;
	private static final EnglishStemmer STEMMER;
	
	static {
		STEMMER = new EnglishStemmer();
		STOP_WORDS = new HashSet<String>();
		
		STOP_WORDS.add("a");
		STOP_WORDS.add("about");
		STOP_WORDS.add("above");
		STOP_WORDS.add("after");
		STOP_WORDS.add("again");
		STOP_WORDS.add("against");
		STOP_WORDS.add("all");
		STOP_WORDS.add("am");
		STOP_WORDS.add("an");
		STOP_WORDS.add("and");
		STOP_WORDS.add("any");
		STOP_WORDS.add("are");
		STOP_WORDS.add("aren't");
		STOP_WORDS.add("as");
		STOP_WORDS.add("at");
		STOP_WORDS.add("be");
		STOP_WORDS.add("because");
		STOP_WORDS.add("been");
		STOP_WORDS.add("before");
		STOP_WORDS.add("being");
		STOP_WORDS.add("below");
		STOP_WORDS.add("between");
		STOP_WORDS.add("both");
		STOP_WORDS.add("but");
		STOP_WORDS.add("by");
		STOP_WORDS.add("can't");
		STOP_WORDS.add("cannot");
		STOP_WORDS.add("could");
		STOP_WORDS.add("couldn't");
		STOP_WORDS.add("did");
		STOP_WORDS.add("didn't");
		STOP_WORDS.add("do");
		STOP_WORDS.add("does");
		STOP_WORDS.add("doesn't");
		STOP_WORDS.add("doing");
		STOP_WORDS.add("don't");
		STOP_WORDS.add("down");
		STOP_WORDS.add("during");
		STOP_WORDS.add("each");
		STOP_WORDS.add("few");
		STOP_WORDS.add("for");
		STOP_WORDS.add("from");
		STOP_WORDS.add("further");
		STOP_WORDS.add("had");
		STOP_WORDS.add("hadn't");
		STOP_WORDS.add("has");
		STOP_WORDS.add("hasn't");
		STOP_WORDS.add("have");
		STOP_WORDS.add("haven't");
		STOP_WORDS.add("having");
		STOP_WORDS.add("he");
		STOP_WORDS.add("he'd");
		STOP_WORDS.add("he'll");
		STOP_WORDS.add("he's");
		STOP_WORDS.add("her");
		STOP_WORDS.add("here");
		STOP_WORDS.add("here's");
		STOP_WORDS.add("hers");
		STOP_WORDS.add("herself");
		STOP_WORDS.add("him");
		STOP_WORDS.add("himself");
		STOP_WORDS.add("his");
		STOP_WORDS.add("how");
		STOP_WORDS.add("how's");
		STOP_WORDS.add("i");
		STOP_WORDS.add("i'd");
		STOP_WORDS.add("i'll");
		STOP_WORDS.add("i'm");
		STOP_WORDS.add("i've");
		STOP_WORDS.add("if");
		STOP_WORDS.add("in");
		STOP_WORDS.add("into");
		STOP_WORDS.add("is");
		STOP_WORDS.add("isn't");
		STOP_WORDS.add("it");
		STOP_WORDS.add("it's");
		STOP_WORDS.add("its");
		STOP_WORDS.add("itself");
		STOP_WORDS.add("let's");
		STOP_WORDS.add("me");
		STOP_WORDS.add("more");
		STOP_WORDS.add("most");
		STOP_WORDS.add("mustn't");
		STOP_WORDS.add("my");
		STOP_WORDS.add("myself");
		STOP_WORDS.add("no");
		STOP_WORDS.add("nor");
		STOP_WORDS.add("not");
		STOP_WORDS.add("of");
		STOP_WORDS.add("off");
		STOP_WORDS.add("on");
		STOP_WORDS.add("once");
		STOP_WORDS.add("only");
		STOP_WORDS.add("or");
		STOP_WORDS.add("other");
		STOP_WORDS.add("ought");
		STOP_WORDS.add("our");
		STOP_WORDS.add("ours");
		STOP_WORDS.add("ourselves");
		STOP_WORDS.add("out");
		STOP_WORDS.add("over");
		STOP_WORDS.add("own");
		STOP_WORDS.add("same");
		STOP_WORDS.add("shan't");
		STOP_WORDS.add("she");
		STOP_WORDS.add("she'd");
		STOP_WORDS.add("she'll");
		STOP_WORDS.add("she's");
		STOP_WORDS.add("should");
		STOP_WORDS.add("shouldn't");
		STOP_WORDS.add("so");
		STOP_WORDS.add("some");
		STOP_WORDS.add("such");
		STOP_WORDS.add("than");
		STOP_WORDS.add("that");
		STOP_WORDS.add("that's");
		STOP_WORDS.add("the");
		STOP_WORDS.add("their");
		STOP_WORDS.add("theirs");
		STOP_WORDS.add("them");
		STOP_WORDS.add("themselves");
		STOP_WORDS.add("then");
		STOP_WORDS.add("there");
		STOP_WORDS.add("there's");
		STOP_WORDS.add("these");
		STOP_WORDS.add("they");
		STOP_WORDS.add("they'd");
		STOP_WORDS.add("they'll");
		STOP_WORDS.add("they're");
		STOP_WORDS.add("they've");
		STOP_WORDS.add("this");
		STOP_WORDS.add("those");
		STOP_WORDS.add("through");
		STOP_WORDS.add("to");
		STOP_WORDS.add("too");
		STOP_WORDS.add("under");
		STOP_WORDS.add("until");
		STOP_WORDS.add("up");
		STOP_WORDS.add("very");
		STOP_WORDS.add("via");
		STOP_WORDS.add("was");
		STOP_WORDS.add("wasn't");
		STOP_WORDS.add("we");
		STOP_WORDS.add("we'd");
		STOP_WORDS.add("we'll");
		STOP_WORDS.add("we're");
		STOP_WORDS.add("we've");
		STOP_WORDS.add("were");
		STOP_WORDS.add("weren't");
		STOP_WORDS.add("what");
		STOP_WORDS.add("what's");
		STOP_WORDS.add("when");
		STOP_WORDS.add("when's");
		STOP_WORDS.add("where");
		STOP_WORDS.add("where's");
		STOP_WORDS.add("which");
		STOP_WORDS.add("while");
		STOP_WORDS.add("who");
		STOP_WORDS.add("who's");
		STOP_WORDS.add("whom");
		STOP_WORDS.add("why");
		STOP_WORDS.add("why's");
		STOP_WORDS.add("with");
		STOP_WORDS.add("without");
		STOP_WORDS.add("won't");
		STOP_WORDS.add("would");
		STOP_WORDS.add("wouldn't");
		STOP_WORDS.add("you");
		STOP_WORDS.add("you'd");
		STOP_WORDS.add("you'll");
		STOP_WORDS.add("you're");
		STOP_WORDS.add("you've");
		STOP_WORDS.add("your");
		STOP_WORDS.add("yours");
		STOP_WORDS.add("yourself");
		STOP_WORDS.add("yourselves");
		STOP_WORDS.add("rrb");
		STOP_WORDS.add("lrb");
	}
	
	public static String normalizeLabel(String label) {
		String norm = label.replace("'", "");
		norm = norm.toLowerCase();
		norm = norm.replaceAll("[-_,;.!?)(/]", " ");
		norm = norm.replaceAll("[0-9]", "");
		norm = norm.replaceAll("\\s+", " ").trim();
		
		return norm;
	}
	
	public static boolean isActivityLabel(String label) {
		if (label == null || label.equals("") || label.length() == 0) {//|| label.endsWith("?")) {
			return false;
		}
		
		boolean isNotDigit = false;
		if (label.startsWith("t") || label.startsWith("p")) {
			CharSequence seq = label.subSequence(1, label.length());
			for (int a = 0; a < seq.length(); a++) {
				char letter = seq.charAt(a);
				if (!(letter >= '0' && letter <= '9')) {
					isNotDigit = true;
					break;
				}
			}
		}
		else {
			isNotDigit = true;
		}
		
		if (!isNotDigit) {
			return false;
		}
		
		return tokenizeAndRemoveStopWords(label).size() != 0;
	}	
	
	/**
	 * This method stems a word using the stemming algorithm by Porter.
	 * 
	 * @param word 	a word
	 * @return 		the stem of the word
	 */
	public static String stemPorter(String word) {
		return STEMMER.getStem(word);
	}
	
	/**
	 * This method checks whether to words are equal or not. Two words are equal if their Levenshtein-Distance is not bigger than 0.15.
	 * @param word1		the first word to compare
	 * @param word2		the second word to compare
	 * @return			true, if the words are equal. false, otherwise.
	 */
	public static boolean areWordsEqual(String word1, String word2) {
		String stem1 = stemPorter(word1);
		String stem2 = stemPorter(word2);
		return stem1.equals(stem2);
	}
	
	/**
	 * This method takes a list of words and groups them. Grouping is done by checking whether two words are equal are not using {@link #areWordsEqual(String, String) areWordsEqual}.  
	 * @param words		the list of words
	 * @return 			a list of sets of words, where each set contains equal words 
	 */
	public static List<Set<String>> groupTerms(Collection<String> words) {
		List<Set<String>> termGroups = new ArrayList<Set<String>>(); 
		
		for (String term : words) {
			List<Set<String>> groups = new ArrayList<Set<String>>();
			for (Set<String> termGroup : termGroups) {
				for (String t : termGroup) {
					if (areWordsEqual(term, t)) {
						groups.add(termGroup);
						break;
					}
				}
			}
			
			if (groups.size() == 0) {
				Set<String> set = new HashSet<String>();
				set.add(term);
				termGroups.add(set);
			}
			else if (groups.size() == 1) {
				groups.get(0).add(term);
			}
			else {
				groups.get(0).add(term);
				for (int a = 1; a < groups.size(); a++) {
					groups.get(0).addAll(groups.get(a));
					termGroups.remove(groups.get(a));
				}
			}
		}
		
		return termGroups;
	}
	
	/**
	 * This method tokenizes a given text using {@code java.util.StringTokenizer}. It also removes all stop words contained in the text. The list of stop words can be configured in the config.xml.
	 * @param text	a text
	 * @return 		a list of all tokens in the text which are no stop words
	 */
	public static List<String> tokenizeAndRemoveStopWords(String text) {
		StringTokenizer tokenizer = new StringTokenizer(normalizeLabel(text));
		List<String> tokens = new ArrayList<String>();
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if (!token.equals("") && !STOP_WORDS.contains(token) && token.length() != 1) {
				tokens.add(token);
			}
		}				
		return tokens;
	}
}

