package bpm.text.cooc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

// TODO: import bpm.matching.process.structure.NeighborhoodUtil;
import bpm.model.Node;
import bpm.model.Process;
import bpm.model.ProcessUtilities;
import bpm.text.TextUtils;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public abstract class Cooccurrence {
	protected HashMap<String, Integer> indecies;
	protected int[][] cooccurrence;
	protected boolean considerNeighborhood;
	protected HashMap<Integer, LinkedList<String>> sortedList;
	protected int size;
	
	public Cooccurrence(String file) {
		this.read(file);
		this.extractSortedLists();
	}

	public Cooccurrence(Collection<Process> processes, boolean considerNeighborhood) {
		this.considerNeighborhood = considerNeighborhood;
		this.getWords(processes);
		this.countCooccurrrence(processes);
		this.extractSortedLists();
	}
	
	private void extractSortedLists() {
		final int[][] cooc = cooccurrence;
		
		this.sortedList = new HashMap<Integer, LinkedList<String>>();
		HashSet<Integer> ins = new HashSet<Integer>();
		for (String w : this.indecies.keySet()) {
			final int index = this.indecies.get(w);
			
			if (!ins.contains(index)) {
				LinkedList<String> is = new LinkedList<String>(this.indecies.keySet());
				Collections.sort(is, new Comparator<String>() {
					@Override
					public int compare(String o1, String o2) {
						int i1 = indecies.get(o1);
						int i2 = indecies.get(o2);
						int v1 = cooc[index][i1];
						int v2 = cooc[index][i2];
						return Integer.compare(v2, v1);
					}
				});				
				this.sortedList.put(index, is);
				ins.add(index);
			}
		}
	}
	
	public Collection<String> getSortedOccurrences(String word) {
		int index = this.indecies.get(word);
		return this.sortedList.get(index);
	}
	
	public int getCooccurrence(String word1, String word2) {
		if (this.indecies.containsKey(word1) && this.indecies.containsKey(word2)) {
			int i1 = this.indecies.get(word1);
			int i2 = this.indecies.get(word2);
			return i1 == i2 ? 0 : this.cooccurrence[i1][i2];
		}
		
		return 0;
	}

	private void countCooccurrrence(Collection<Process> processes) {
		this.cooccurrence = new int[this.size][this.size];
		
		for (Process process : processes) {
			for (Node ac : ProcessUtilities.getActivities(process)) {
				LinkedList<String> words = this.extractOccurences(ac);
				for (int a = 0; a < words.size() - 1; a++) {
					String w1 = words.get(a);
					int i1 = this.indecies.get(w1);
					for (int b = a + 1; b < words.size(); b++) {
						String w2 = words.get(b);
						int i2 = this.indecies.get(w2);
						this.cooccurrence[i1][i2]++;
						this.cooccurrence[i2][i1]++;
					}
				}
			}
		}
	}

	private LinkedList<String> extractOccurences(Node ac) {
		LinkedList<Node> nodes = new LinkedList<Node>();
		nodes.add(ac);
		if (this.considerNeighborhood) {
			// TODO nodes.addAll(NeighborhoodUtil.determineNeighborhood(ac));
		}
		
		LinkedList<String> words = new LinkedList<String>();
		for (Node n : nodes) {
			for (String w : TextUtils.tokenizeAndRemoveStopWords(n.getLabel())) {
				if (!words.contains(w)) {
					words.add(w);
				}
			}
		}
		
		return words;
	}

	protected abstract void getWords(Collection<Process> processes);
	
	public LinkedList<String> getTopKindex(String word1, String word2, int k) {
		LinkedList<String> kwords = new LinkedList<String>();
		
		int index1 = this.indecies.get(word1);
		
		int a = 0;
		
		for (String word : this.sortedList.get(index1)) {
			int index2 = this.indecies.get(word);
			if (index1 != index2) {
				kwords.add(word);
				a++;
			}
			
			if (a == k) {
				break;
			}
		}
		
		return kwords;
	}
	
	public void write(String filename) {
		try {
			File file = new File(filename);
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			
			for (String w : this.indecies.keySet()) {
				writer.write(w);
				writer.write(";");
				writer.write(Integer.toString(this.indecies.get(w)));
				writer.write("\n");
			}
			
			writer.write("end\n");
			
			for (int a = 0; a < this.cooccurrence.length; a++) {
				String prefix = "";
				for (int b = 0; b < this.cooccurrence.length; b++) {
					writer.write(prefix);
					writer.write(Integer.toString(this.cooccurrence[a][b]));
					prefix = ";";
				}
				writer.write("\n");
			}	
			writer.write("end");
			
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void read(String filename) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));
			String line = null;
			this.indecies = new HashMap<String, Integer>();
			this.size = 0;
			while(!(line = reader.readLine()).equals("end")) {
				String[] parts = line.split(";");
				int i = Integer.parseInt(parts[1]);
				this.size = Math.max(i, this.size);
				this.indecies.put(parts[0], i);
			}
			this.size++;		
			
			this.cooccurrence = new int[this.size][this.size];
			
			int a = 0;
			while (!(line = reader.readLine()).equals("end")) {
				String[] parts = line.split(";");
				for (int b = 0; b < parts.length; b++) {
					this.cooccurrence[a][b] = Integer.parseInt(parts[b]);
				}				
				a++;
			}
			
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
