package bpm.text.cooc;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;

import bpm.model.Node;
import bpm.model.Process;
import bpm.model.ProcessUtilities;
import bpm.text.TextUtils;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

class WordExtractionUtils {
	
	static int getWords(Collection<Process> processes, HashMap<String, Integer> indecies) {
		LinkedList<String> words = new LinkedList<String>();
		for (Process process : processes) {
			for (Node ac : ProcessUtilities.getActivities(process)) {
				for (String word : TextUtils.tokenizeAndRemoveStopWords(ac.getLabel())) {
					if (!words.contains(word)) {
						words.add(word);
					}
				}
			}
		}
		
		Collections.sort(words);
		for (String word : words) {
			indecies.put(word, indecies.size());
		}
		return indecies.size();
	}
	
	static int getPorterWords(Collection<Process> processes, HashMap<String, Integer> indecies) {
		HashMap<String, String> stems = new HashMap<String, String>();
		HashMap<String, Integer> index = new HashMap<String, Integer>();
		
		for (Process process : processes) {
			for (Node ac : ProcessUtilities.getActivities(process)) {
				for (String word : TextUtils.tokenizeAndRemoveStopWords(ac.getLabel())) {
					if (!stems.containsKey(word)) {
						String s = TextUtils.stemPorter(word);
						stems.put(word, s);
						
						if (!index.containsKey(s)) {
							index.put(s, index.size());
						}
					}
				}
			}
		}
		
		for (String word : stems.keySet()) {
			String stem = stems.get(word);
			indecies.put(word, index.get(stem));
		}
		
		return index.size();
	}
}
