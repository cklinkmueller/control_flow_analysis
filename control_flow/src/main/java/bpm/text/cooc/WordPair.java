package bpm.text.cooc;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class WordPair {
	private String word1;
	private String word2;
	private long cooc;
	
	public WordPair(String word1, String word2) {
		if (word1.compareTo(word2) > 0) {
			this.word1 = word2;
			this.word2 = word1;			
		}
		else {
			this.word1 = word1;
			this.word2 = word2;		
		}		
	}

	public String getWord1() {
		return word1;
	}

	public void setWord1(String word1) {
		this.word1 = word1;
	}

	public String getWord2() {
		return word2;
	}

	public void setWord2(String word2) {
		this.word2 = word2;
	}
	
	public long getCooccurrence() {
		return this.cooc;
	}
	
	public void setCooccurrence(long cooccurrence) {
		this.cooc = cooccurrence;
	}
}
