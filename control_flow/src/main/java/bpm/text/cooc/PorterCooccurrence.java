package bpm.text.cooc;

import java.util.Collection;
import java.util.HashMap;

import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class PorterCooccurrence extends Cooccurrence {
	public PorterCooccurrence(Collection<Process> processes, boolean considerNeighborhood) {
		super(processes, considerNeighborhood);
	}
	
	public PorterCooccurrence(String file) {
		super(file);
	}

	protected void getWords(Collection<Process> processes) {
		this.indecies = new HashMap<String, Integer>();
		this.size = WordExtractionUtils.getPorterWords(processes, this.indecies);
	}
}
