package bpm.text.stem;

import org.tartarus.snowball.ext.germanStemmer;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class GermanStemmer extends PorterStemmer {
	
	public GermanStemmer() {
		super(new germanStemmer());
	}
	
}
