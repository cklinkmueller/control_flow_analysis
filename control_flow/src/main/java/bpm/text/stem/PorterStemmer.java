package bpm.text.stem;

import org.tartarus.snowball.SnowballStemmer;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

abstract class PorterStemmer implements Stemmer {
	private SnowballStemmer stemmer;
	
	public PorterStemmer(SnowballStemmer stemmer) {
		this.stemmer = stemmer;
	} 
	
	
	@Override
	public String getStem(String word) {
		this.stemmer.setCurrent(word);
		this.stemmer.stem();
		return this.stemmer.getCurrent();
	}

}
