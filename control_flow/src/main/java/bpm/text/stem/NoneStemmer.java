package bpm.text.stem;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class NoneStemmer implements Stemmer {

	@Override
	public String getStem(String word) {
		return word;
	}

}
