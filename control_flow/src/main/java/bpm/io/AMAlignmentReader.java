package bpm.io;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import bpm.model.Alignment;
import bpm.model.Node;
import bpm.model.Process;
import bpm.model.ProcessUtilities;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class AMAlignmentReader extends AlignmentReader {
	private DocumentBuilder docBuilder;
	private XPath xpath;
	
	public AMAlignmentReader() {
		try {
			this.docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			this.xpath = XPathFactory.newInstance().newXPath();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void readFile(String filename, List<Process> processes, LinkedList<Alignment> alignments) {
		File folder = new File(filename);
		File ra = null;
		File source = null;
		File target = null;
		
		for (File f : folder.listFiles()) {
			if (f.getName().contains("source")) {
				source = f;
			}
			else if (f.getName().contains("target")) {
				target = f;
			}
			else {
				ra = f;
			}
		}
		
		Process p1 = this.getProcess(processes, source);
		Process p2 = this.getProcess(processes, target);
		
		if (p1 == null || p2 == null) {
			return;
		}
		
		Alignment alignment = new Alignment(p1, p2);
		Collection<Node> acs1 = ProcessUtilities.getActivities(p1);
		Collection<Node> acs2 = ProcessUtilities.getActivities(p2);
		
		this.readCorrespondences(alignment, acs1, acs2, ra);
		
		alignments.add(alignment);
	}

	private void readCorrespondences(Alignment alignment, Collection<Node> acs1, Collection<Node> acs2, File file) {
		Document doc;
		try {
			doc = this.docBuilder.parse(file);
			XPathExpression expr = this.xpath.compile("./Alignment/map/Cell");
			NodeList cells = (NodeList)expr.evaluate(doc.getDocumentElement(), XPathConstants.NODESET);
			for (int a = 0; a < cells.getLength(); a++) {
				org.w3c.dom.Node n = (org.w3c.dom.Node)cells.item(a);
				this.addCorrespondence(n, alignment, acs1, acs2);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void addCorrespondence(org.w3c.dom.Node n, Alignment alignment, Collection<Node> acs1, Collection<Node> acs2) throws XPathExpressionException {
		XPathExpression expr = this.xpath.compile("./entity1/@resource");
		String url1 = (String)expr.evaluate(n, XPathConstants.STRING);
		
		expr = this.xpath.compile("./entity2/@resource");
		String url2 = (String)expr.evaluate(n, XPathConstants.STRING);
		
		String[] parts1 = url1.split("/");
		String[] parts2 = url2.split("/");
		String id1 = parts1[parts1.length - 1];
		String id2 = parts2[parts2.length - 1];
		
		for (Node ac1 : acs1) {
			if (ac1.getOldId().equals(id1)) {
				for (Node ac2 : acs2) {
					if (ac2.getOldId().equals(id2)) {
						alignment.setCorresponding(ac1, ac2, true);
					}
				}
			}
		}
	}

	private Process getProcess(List<Process> processes, File file) {
		try {
			Document doc = this.docBuilder.parse(file);
			XPathExpression expr = this.xpath.compile("/epml/epc/@name");
			String name = (String)expr.evaluate(doc.getDocumentElement(), XPathConstants.STRING);
			
			for (Process p : processes) {
				if (p.getName().equals(name)) {
					return p;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Alignment> read(String path, List<Process> processes) {
		LinkedList<Alignment> alignments = new LinkedList<Alignment>();
		
		File file = new File(path);		
		this.addAlignments(file, processes, alignments);
		
		return alignments;
	}

	private void addAlignments(File file, List<Process> processes, LinkedList<Alignment> alignments) {
		if (file.isDirectory()) {
			if (file.getName().contains("testcase")) {
				this.readFile(file.getAbsolutePath(), processes, alignments);
			}
			else {
				for (File f : file.listFiles()) {
					this.addAlignments(f, processes, alignments);
				}
			}
		}
	}
	
}
