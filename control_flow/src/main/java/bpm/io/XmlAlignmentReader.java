package bpm.io;

import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import bpm.model.Alignment;
import bpm.model.Node;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class XmlAlignmentReader extends AlignmentReader {
	private DocumentBuilder docBuilder;
	private XPath xpath;
	
	public XmlAlignmentReader() {
		try {
			this.docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			this.xpath = XPathFactory.newInstance().newXPath();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	@Override
	protected void readFile(String filename, List<Process> processes, LinkedList<Alignment> alignments) {
		File file = new File(filename);
		
		Document doc;
		try {
			doc = this.docBuilder.parse(file);
			XPathExpression expr = this.xpath.compile("./ProcessPairs/ProcessPair");
			NodeList list = (NodeList)expr.evaluate(doc, XPathConstants.NODESET);
			
			for (int a = 0; a < list.getLength(); a++) {
				Element e = (Element)list.item(a);
				alignments.add(this.readAlignment(e, processes));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private Alignment readAlignment(Element alignmentElement, List<Process> processes) throws XPathExpressionException {
		Process p1 = this.getProcess(processes, alignmentElement.getAttribute("process1"));
		Process p2 = this.getProcess(processes, alignmentElement.getAttribute("process2"));
		
		if (p1 == null || p2 == null) {
			return null;
		}
		
		Alignment al = new Alignment(p1, p2);
		
		XPathExpression expr = this.xpath.compile("./Cluster");
		NodeList list = (NodeList)expr.evaluate(alignmentElement, XPathConstants.NODESET);
		
		for (int a = 0; a < list.getLength(); a++) {
			Element clusterElement = (Element)list.item(a);
			this.readCluster(clusterElement, p1, p2, al);
		}
				
		return al;
	}
	private void readCluster(Element clusterElement, Process p1, Process p2, Alignment al) throws XPathExpressionException {
		HashSet<Node> nodes1 = new HashSet<Node>();
		HashSet<Node> nodes2 = new HashSet<Node>();
		
		XPathExpression expr = this.xpath.compile("./Node");
		NodeList list = (NodeList)expr.evaluate(clusterElement, XPathConstants.NODESET);
		for (int a = 0; a < list.getLength(); a++) {
			Element el = (Element)list.item(a);
			long pId = Long.parseLong(el.getAttribute("process"));
			
			if (pId == p1.getId()) {
				nodes1.add(this.getNode(p1, el.getAttribute("id")));
			}
			else {
				nodes2.add(this.getNode(p2, el.getAttribute("id")));
			}
		}
		
		for (Node n1 : nodes1) {
			for (Node n2 : nodes2) {
				al.setCorresponding(n1, n2, true);
			}
		}
	}
	
	private Node getNode(Process p, String id) {
		for (Node n : p.getNodes()) {
			if (n.getOldId().equals(id)) {
				return n;
			}
		}
		return null;
	}
	
	private Process getProcess(List<Process> processes, String id) {
		for (Process p : processes) {
			if (p.getId() == Long.parseLong(id)) {
				return p;
			}
		}
		return null;
	}

}
