package bpm.io;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import bpm.model.Alignment;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public abstract class AlignmentReader {
	protected abstract void readFile(String filename, List<Process> processes, LinkedList<Alignment> alignments);
	
	public List<Alignment> read(String path, List<Process> processes) {
		LinkedList<Alignment> alignments = new LinkedList<Alignment>();
		
		File file = new File(path);
		this.readAlignments(file, alignments, processes);
		
		return alignments;
	}

	private void readAlignments(File file, LinkedList<Alignment> alignments, List<Process> processes) {
		if (file.isFile()) {
			this.readFile(file.getAbsolutePath(), processes, alignments);
		}
		else {
			for (File f : file.listFiles()) {
				this.readAlignments(f, alignments, processes);
			}
		}
	}
}
