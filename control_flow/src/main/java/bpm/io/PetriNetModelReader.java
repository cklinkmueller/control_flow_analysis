package bpm.io;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import bpm.model.Edge;
import bpm.model.EdgeTypes;
import bpm.model.Node;
import bpm.model.NodeTypes;
import bpm.model.Process;
import bpm.model.ProcessTypes;
import bpm.text.TextUtils;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class PetriNetModelReader extends ModelReader {
	private static long ID = 0;
	private DocumentBuilder documentBuilder;
	
	public PetriNetModelReader() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			this.documentBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();			
		}
	}

	@Override
	public List<Process> readFile(String filename) {
		Element rootElement = this.readDocument(filename);
		
		LinkedList<Process> processes = new LinkedList<Process>();
		NodeList netElements = rootElement.getElementsByTagName("net");
		for (int a = 0; a < netElements.getLength(); a++) {
			Element netEl = (Element)netElements.item(a);
			Process net = this.readNet(netEl, filename);
			processes.add(net);
		}
		
		return processes;
	}

	private Process readNet(Element netEl, String filename) {
		Process net = this.transformNet(netEl, filename);
		net.setId(ID++);
		
		HashMap<String, Node> nodes = new HashMap<String, Node>();
		this.readNodes(net, netEl.getElementsByTagName("place"), true, nodes);
		this.readNodes(net, netEl.getElementsByTagName("transition"), false, nodes);
		this.readArcs(net, netEl.getElementsByTagName("arc"), nodes);
		
		return net;
	}
	
	private Process transformNet(Element netEl, String filename) {
		try {
			Process net = new Process();
			net.setType(ProcessTypes.PETRI);
			
			NodeList childNodes = netEl.getChildNodes();
			for (int a = 0; a < childNodes.getLength(); a++) {
				org.w3c.dom.Node node = childNodes.item(a);
				
				if (Element.class.isInstance(node) && node.getNodeName().equals("name")) {
					Element element = (Element)node;
					NodeList texts = element.getElementsByTagName("text");
					if (texts.getLength() == 1) {
						net.setName(texts.item(0).getTextContent());
					}
					else {
						this.setPetriNetName(net, filename);
					}
				}
				else {
					this.setPetriNetName(net, filename);
				}
			}
								
			return net;
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return null;
	}
	
	private void setPetriNetName(Process net, String filename) {
		int i0 = filename.lastIndexOf('\\');
		int i1 = filename.lastIndexOf('/');
		
		if (i0 == -1 && i1 == -1) {
			net.setName(filename.replace(".pnml", ""));
		}
		else if (i0 > i1) {
			net.setName(filename.substring(i0 + 1).replace(".pnml", ""));
		}
		else {
			net.setName(filename.substring(i1 + 1).replace(".pnml", ""));
		}
		net.setName(net.getName());
	}

	private void readNodes(Process net, NodeList elements, boolean isPlace, HashMap<String, Node> nodes) {
		for (int a = 0; a < elements.getLength(); a++) {
			Element element = (Element)elements.item(a);
			String id = element.getAttribute("id");
			String name = "";
			
			Element nameElement = (Element)element.getElementsByTagName("name").item(0);
			if (nameElement != null) {
				Element textElement = (Element)nameElement.getElementsByTagName("text").item(0);
				if (textElement != null) {
					name = textElement.getTextContent().toLowerCase();
					if (!TextUtils.isActivityLabel(name)) {
						name = "";
					}
				}
			}
			name = TextUtils.normalizeLabel(name);
			
			Node node = new Node();
			node.setLabel(name);
			node.setType(isPlace ? NodeTypes.PETRI_PLACE : NodeTypes.PETRI_TRANSITION);
			net.addNode(node);
			node.setProcess(net);
			node.setOldId(id);
			node.setId(ID++);
			nodes.put(id, node);
			
			Element graphicsElement = (Element)element.getElementsByTagName("graphics").item(0);
			if (graphicsElement != null) {
				Element positionElement = (Element)element.getElementsByTagName("position").item(0);
				node.setX(Double.parseDouble(positionElement.getAttribute("x")));
				node.setY(Double.parseDouble(positionElement.getAttribute("y")));
				
				Element dimensionElement = (Element)element.getElementsByTagName("dimension").item(0);
				node.setHeight(Double.parseDouble(dimensionElement.getAttribute("x")));
				node.setWidth(Double.parseDouble(dimensionElement.getAttribute("y")));
			}
		}
	}
	
	private void readArcs(Process net, NodeList elements, HashMap<String, Node> nodes) {
		for (int a = 0; a < elements.getLength(); a++) {
			Element element = (Element)elements.item(a);
			String s_id = element.getAttribute("source");
			String t_id = element.getAttribute("target");
			
			Node source = nodes.get(s_id);
			Node target = nodes.get(t_id);
			Edge edge = new Edge(source, target);
			edge.setId(ID++);
			edge.setProcess(net);
			edge.setType(EdgeTypes.PETRI_ARC);		
			net.addEdge(edge);
		}
	}

	private Element readDocument(String filename) {
		File file = new File(filename);
		
		try {
			Document document = this.documentBuilder.parse(file);
			document.getDocumentElement().normalize();
			return document.getDocumentElement();
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
