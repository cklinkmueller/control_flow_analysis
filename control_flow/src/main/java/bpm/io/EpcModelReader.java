package bpm.io;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import bpm.model.Edge;
import bpm.model.EdgeTypes;
import bpm.model.Node;
import bpm.model.NodeTypes;
import bpm.model.Process;
import bpm.model.ProcessTypes;
import bpm.text.TextUtils;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class EpcModelReader extends ModelReader {
	private static long ID;
	private DocumentBuilder documentBuilder;
	
	public EpcModelReader() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			this.documentBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();			
		}
	}

	@Override
	public List<Process> readFile(String filename) {
		Element rootElement = this.readDocument(filename);
		
		LinkedList<Process> processes = new LinkedList<Process>();
		NodeList epcElements = rootElement.getElementsByTagName("epc");
		for (int a = 0; a < epcElements.getLength(); a++) {
			Element epcEl = (Element)epcElements.item(a);
			processes.add(this.readEpc(epcEl, a, filename));
		}
		
		return processes;
	}

	private Process readEpc(Element epcEl, int pos, String filename) {
		Process process = new Process();
		process.setId(ID++);
		
		String name = "";
		long id = 0;
		
		if (epcEl.hasAttribute("name")) {
			name = epcEl.getAttribute("name");
		}
		else {
			int index = filename.lastIndexOf("/");
			name = filename.substring(index + 1);
			name = name.replace(".epml", "");
			name += " " + pos;
		}
		if (epcEl.hasAttribute("epcId")) {
			id = Long.parseLong(epcEl.getAttribute("epcId"));
			process.setId(id);
		}
		
		
		process.setName(name);
		process.setType(ProcessTypes.EPC);
		
		HashMap<String, Node> nodes = new HashMap<String, Node>();
		this.readNodes(process, epcEl, nodes);
		this.readArcs(process, epcEl, nodes);
		
		return process;
	}

	private void readArcs(Process process, Element epcEl, HashMap<String, Node> nodes) {
		NodeList arcList = epcEl.getElementsByTagName("arc");
		for (int a = 0; a < arcList.getLength(); a++) {
			Element arcEl = (Element)arcList.item(a);
			
			NodeList flowList = arcEl.getElementsByTagName("flow");
			for (int b = 0; b < flowList.getLength(); b++) {
				Element flow = (Element)flowList.item(b);
				Node source = nodes.get(flow.getAttribute("source"));
				Node target = nodes.get(flow.getAttribute("target"));
				Edge edge = new Edge(source, target);
				edge.setId(ID++);
				edge.setType(EdgeTypes.EPC_ARC);
				edge.setProcess(process);
				process.addEdge(edge);
			}
		}
	}

	private void readNodes(Process process, Element epcEl, HashMap<String, Node> nodes) {
		this.readNodes(process, epcEl, nodes, "function", NodeTypes.EPC_ACTIVITY);
		this.readNodes(process, epcEl, nodes, "event", NodeTypes.EPC_EVENT);
		this.readNodes(process, epcEl, nodes, "and", NodeTypes.EPC_AND);
		this.readNodes(process, epcEl, nodes, "or", NodeTypes.EPC_OR);
		this.readNodes(process, epcEl, nodes, "xor", NodeTypes.EPC_XOR);
	}

	private void readNodes(Process process, Element epcEl, HashMap<String, Node> nodes, String elName, String type) {
		NodeList nodelist = epcEl.getElementsByTagName(elName);
		for (int a = 0; a < nodelist.getLength(); a++) {
			Element el = (Element)nodelist.item(a);
			
			Node node = new Node();
			node.setId(ID++);
			node.setProcess(process);
			node.setType(type);
			
			String label = "";
			NodeList names = el.getElementsByTagName("name");
			for (int b = 0; b < names.getLength(); b++) {
				Element name = (Element)names.item(b);
				label = name.getTextContent().replace("\n", " ");
			}
			label = TextUtils.normalizeLabel(label);
			node.setLabel(label);
			
			process.addNode(node);
			
			String oldId = el.getAttribute("id");
			nodes.put(oldId, node);
			node.setOldId(oldId);			
			
			NodeList graphics = el.getElementsByTagName("position");
			for (int b = 0; b < graphics.getLength(); b++) {
				Element graphic = (Element)graphics.item(b);
				node.setHeight(Double.parseDouble(graphic.getAttribute("height")));
				node.setWidth(Double.parseDouble(graphic.getAttribute("width")));
				node.setX(Double.parseDouble(graphic.getAttribute("x")));
				node.setY(Double.parseDouble(graphic.getAttribute("y")));
			}
		}		
	}

	private Element readDocument(String filename) {
		File file = new File(filename);
		
		try {
			Document document = this.documentBuilder.parse(file);
			document.getDocumentElement().normalize();
			return document.getDocumentElement();
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
