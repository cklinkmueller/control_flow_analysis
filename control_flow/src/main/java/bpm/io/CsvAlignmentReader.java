package bpm.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

import bpm.model.Alignment;
import bpm.model.Node;
import bpm.model.Process;
import bpm.model.ProcessUtilities;
import bpm.text.TextUtils;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class CsvAlignmentReader extends AlignmentReader {

	@Override
	protected void readFile(String filename, List<Process> processes, LinkedList<Alignment> alignments) {
		File file = new File(filename);
		
		Alignment alignment = null;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
			
			String line = reader.readLine();
			Process process1 = this.getProcess(processes, line);
			
			line = reader.readLine();
			Process process2 = this.getProcess(processes, line);
			
			if (process1 == null || process2 == null) {
				return;
			}
			
			alignment = new Alignment(process1, process2);
			List<Node> acs1 = ProcessUtilities.getActivities(process1);
			List<Node> acs2 = ProcessUtilities.getActivities(process2);
			
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.length() != 0) {
					String[] labels = line.split(",");
					 
					String label1 = TextUtils.normalizeLabel(labels[0]);
					String label2 = TextUtils.normalizeLabel(labels[1]);
				
					int pos = 0;
					while (pos < acs1.size() * acs2.size()) {
						Node ac1 = acs1.get(pos / acs2.size());
						Node ac2 = acs2.get(pos % acs2.size());
						
						if (ac1.getLabel().equals(label1) && ac2.getLabel().equals(label2) && !alignment.areCorresponding(ac1, ac2)) {
							alignment.setCorresponding(ac1, ac2, true);
							break;
						}
						
						pos++;
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		alignments.add(alignment);
	}

	private Process getProcess(List<Process> processes, String line) {
		if (line != null) {
			String name = line.split("\\.")[0];
			for (Process p : processes) {
				if (p.getName().equals(name)) {
					return p;
				}
			}
		}
		return null;
	}

}
