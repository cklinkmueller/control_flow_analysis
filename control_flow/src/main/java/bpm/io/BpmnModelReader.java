package bpm.io;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import bpm.model.Edge;
import bpm.model.EdgeTypes;
import bpm.model.Node;
import bpm.model.NodeTypes;
import bpm.model.Process;
import bpm.model.ProcessTypes;
import bpm.text.TextUtils;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkm�ller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class BpmnModelReader extends ModelReader {
	private DocumentBuilder documentBuilder;
	
	public BpmnModelReader() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			this.documentBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();			
		}
	}

	@Override
	public List<Process> readFile(String filename) {
		LinkedList<Process> processes = new LinkedList<Process>();
		File file = new File(filename);
		
		try {
			Document document = this.documentBuilder.parse(file);
			Element root = (Element)document.getDocumentElement();
			
			Process process = new Process(file.getName().replace(".bpmn",""));
			process.setType(ProcessTypes.BPMN);
			
			HashMap<String, Node> nodes = new HashMap<String, Node>();
			this.readNodes(process, root, nodes);
			this.readEdges(process, root, nodes);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return processes;
	}

	private void readEdges(Process process, Element root, HashMap<String, Node> nodes) {
		this.readEdges(process, root, nodes, "messageFlow");
		this.readEdges(process, root, nodes, "sequenceFlow");	
	}

	private void readEdges(Process process, Element root, HashMap<String, Node> nodes, String tag) {
		NodeList list = root.getElementsByTagName(tag);
		for (int a = 0; a < list.getLength(); a++) {
			Element el = (Element)list.item(a);
			String source = el.getAttribute("sourceRef");
			String target = el.getAttribute("targetRef");
			Edge edge = new Edge(nodes.get(source), nodes.get(target));
			edge.setProcess(process);
			edge.setType(EdgeTypes.BPMN_FLOW);
			process.addEdge(edge);			
		}
	}

	private void readNodes(Process process, Element root, HashMap<String, Node> nodes) {
		this.readNodeType(process, root, nodes, "task", NodeTypes.BPMN_ACTIVITY);
		this.readNodeType(process, root, nodes, "parallelGateway", NodeTypes.BPMN_PARALLEL_GATEWAY);
		this.readNodeType(process, root, nodes, "intermediateCatchEvent", NodeTypes.BPMN_INTERMEDIATE_EVENT);
		this.readNodeType(process, root, nodes, "exclusiveGateway", NodeTypes.BPMN_EXCLUSIVE_GATEWAY);
		this.readNodeType(process, root, nodes, "startEvent", NodeTypes.BPMN_START_EVENT);
		this.readNodeType(process, root, nodes, "endEvent", NodeTypes.BPMN_END_EVENT);
		this.readNodeType(process, root, nodes, "eventBasedGateway", NodeTypes.BPMN_EXCLUSIVE_GATEWAY);
	}

	private void readNodeType(Process process, Element root, HashMap<String, Node> nodes, String tag, String type) {
		NodeList list = root.getElementsByTagName(tag);
		for (int a = 0; a < list.getLength(); a++) {
			Element el = (Element)list.item(a);
			String oldId = el.getAttribute("id");
			String name = el.getAttribute("name").toLowerCase().replace("\n","").replace("�", "oe").replace("�", "ue").replace("�", "ae");
			name = TextUtils.normalizeLabel(name);
			Node node = new Node(name);
			node.setType(type);
			node.setProcess(process);
			node.setOldId(oldId);
			process.addNode(node);
			nodes.put(oldId, node);
		}		
	}
	
}
