package bpm.io;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import bpm.model.Edge;
import bpm.model.Node;
import bpm.model.Process;
import bpm.text.TextUtils;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class XmlModelReader extends ModelReader {
	private static long ID = 0;
	private DocumentBuilder docBuilder;
	private XPath xpath;
	private final NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
	
	public XmlModelReader() {
		try {
			this.docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			this.xpath = XPathFactory.newInstance().newXPath();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Process> readFile(String filename) {
		File file = new File(filename);
		LinkedList<Process> processes = new LinkedList<Process>();
		
		Document doc;
		try {
			doc = this.docBuilder.parse(file);
			XPathExpression expr = this.xpath.compile("./Process");
			NodeList proEls = (NodeList)expr.evaluate(doc.getDocumentElement(), XPathConstants.NODESET);
			for (int a = 0; a < proEls.getLength(); a++) {
				Element n = (Element)proEls.item(a);
				Process p = this.readProcess(n);
				processes.add(p);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return processes;
	}

	private Process readProcess(Element e) throws XPathExpressionException, ParseException {
		Process p = new Process(e.getAttribute("name"));
		p.setId(Long.parseLong(e.getAttribute("id")));
		p.setType(e.getAttribute("type"));
		
		HashMap<String, Node> nodes = this.readNodes(p, e);
		this.readEdges(p, e, nodes);
		
		return p;
	}

	private void readEdges(Process p, Element processElement, HashMap<String, Node> nodes) throws XPathExpressionException {
		XPathExpression expr = this.xpath.compile("./Edge");
		NodeList edgeElements = (NodeList)expr.evaluate(processElement, XPathConstants.NODESET);
		
		for (int a = 0; a < edgeElements.getLength(); a++) {
			Element el = (Element)edgeElements.item(a);
			Edge edge = new Edge();
			edge.setId(ID++);
			edge.setSource(nodes.get(el.getAttribute("source")));
			edge.setTarget(nodes.get(el.getAttribute("target")));
			edge.setType(el.getAttribute("type"));
			
			edge.setProcess(p);
			p.addEdge(edge);
		}
	}

	private HashMap<String, Node> readNodes(Process p, Element processElement) throws XPathExpressionException, ParseException {
		XPathExpression expr = this.xpath.compile("./Node");
		NodeList nodeElements = (NodeList)expr.evaluate(processElement, XPathConstants.NODESET);
		
		expr = this.xpath.compile("./Label");
		
		HashMap<String, Node> nodes = new HashMap<String, Node>();
		
		for (int a = 0; a < nodeElements.getLength(); a++) {
			Element el = (Element)nodeElements.item(a);
			Node node = new Node();
			node.setId(ID++);
			node.setOldId(el.getAttribute("id"));
			node.setType(el.getAttribute("type"));
			node.setHeight(this.format.parse(el.getAttribute("height")).doubleValue());
			node.setWidth(this.format.parse(el.getAttribute("width")).doubleValue());
			node.setX(this.format.parse(el.getAttribute("x")).doubleValue());
			node.setY(this.format.parse(el.getAttribute("y")).doubleValue());
			
			Element label = (Element)expr.evaluate(el, XPathConstants.NODE);
			if (label == null) {
				node.setLabel("");
			}
			else {
				node.setLabel(TextUtils.normalizeLabel(label.getTextContent()));
			}
			
			nodes.put(node.getOldId(), node);
			p.addNode(node);
			node.setProcess(p);
		}
		
		return nodes;
	}

}
