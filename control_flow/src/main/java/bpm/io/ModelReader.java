package bpm.io;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public abstract class ModelReader {
	public abstract List<Process> readFile(String filename);
	
	public List<Process> read(String path) {
		LinkedList<Process> processes = new LinkedList<Process>();
		
		File file = new File(path);
		this.readFolder(file, processes);
		
		return processes;
	}

	private void readFolder(File file, LinkedList<Process> processes) {
		if (file.isDirectory()) {
			for (File f : file.listFiles()) {
				this.readFolder(f, processes);
			}
		}
		else {
			processes.addAll(this.readFile(file.getAbsolutePath()));
		}
	}
}
