package bpm.model;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class EdgeTypes {	
	public final static String PETRI_ARC = "petri net edge";
	public final static String EPC_ARC = "epc arc";
	public final static String BPMN_FLOW = "bpmn flow";
	
	public static String getFlowType(Process process) {
		switch (process.getType()) {
			case ProcessTypes.BPMN : return BPMN_FLOW;
			case ProcessTypes.EPC : return EPC_ARC;
			default : return PETRI_ARC;
		}
		
	}

}
