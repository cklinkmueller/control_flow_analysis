package bpm.model;

import java.util.HashMap;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class Alignment {
	private HashMap<Long, Integer> indecies1;
	private HashMap<Long, Integer> indecies2;
	private boolean[][] corresponding;
	
	private Process process1;
	private Process process2;
	private int numberOfCorrespondences;
	
	public Alignment(Process process1, Process process2) {
		this.process1 = process1;
		this.process2 = process2;
		
		this.indecies1 = new HashMap<Long, Integer>();
		this.indecies2 = new HashMap<Long, Integer>();
		
		this.addActivities(this.process1, this.indecies1);
		this.addActivities(this.process2, this.indecies2);
		
		this.corresponding = new boolean[this.indecies1.size()][this.indecies2.size()];
		this.numberOfCorrespondences = 0;
	}

	private void addActivities(Process process, HashMap<Long, Integer> indecies) {
		for (Node ac : ProcessUtilities.getActivities(process)) {
			indecies.put(ac.getId(), indecies.size());
		}
	}
	
	public Process getProcess1() {
		return this.process1;
	}
	
	public Process getProcess2() {
		return this.process2;
	}
	
	public void setCorresponding(Node ac1, Node ac2, boolean value) {
		Integer id1 = this.indecies1.get(ac1.getId());
		if (id1 == null) {
			return;
		}
		
		Integer id2 = this.indecies2.get(ac2.getId());
		if (id2 == null) {
			return;
		}
		
		if (this.corresponding[id1][id2] != value) {
			if (value) {
				this.numberOfCorrespondences++;
			}
			else {
				this.numberOfCorrespondences--;
			}
		}
		
		this.corresponding[id1][id2] = value;
	}
	
	public boolean areCorresponding(Node ac1, Node ac2) {
		Integer id1 = this.indecies1.get(ac1.getId());
		if (id1 == null) {
			return false;
		}
		
		Integer id2 = this.indecies2.get(ac2.getId());
		if (id2 == null) {
			return false;
		}
		
		return this.corresponding[id1][id2];
	}

	public int numberOfCorrespondences() {
		return this.numberOfCorrespondences;
	}
}
