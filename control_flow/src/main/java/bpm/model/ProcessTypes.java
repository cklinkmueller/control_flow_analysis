package bpm.model;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class ProcessTypes {
	public final static String BPMN = "BPMN";
	public final static String EPC = "EPK";
	public final static String PETRI = "Petri Netz";
	
	public static boolean hasType(Process process, String type) {
		return process.getType().equals(type);
	}
}
