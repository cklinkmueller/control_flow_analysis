package bpm.model;

import bpm.text.TextUtils;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class NodeTypes {
	public final static String PETRI_TRANSITION = "petri net transition";
	public final static String PETRI_PLACE = "petri net place";
	public final static String BPMN_ACTIVITY = "bpmn activity";
	public final static String BPMN_START_EVENT = "bpmn start event";
	public final static String BPMN_END_EVENT = "bpmn end event";
	public final static String BPMN_PARALLEL_GATEWAY = "bpmn parallel gateway";
	public final static String BPMN_EXCLUSIVE_GATEWAY = "bpmn exclusive gateway";
	public static final String BPMN_INTERMEDIATE_EVENT = "bpmn intermediate event";
	public static final String BPMN_INCLUSIVE_GATEWAY = "bpmn inclusive gateway";
	public final static String EPC_XOR = "epc xor";
	public final static String EPC_AND = "epc and";
	public final static String EPC_ACTIVITY = "epc activity";
	public final static String EPC_EVENT = "epc event";
	public final static String EPC_OR = "epc or";
	public final static String EPC_INFORMATION_OBJECT = "epc information object";
	public final static String EPC_ORGANIZATION_UNIT = "epc organization unit";
	
	public static boolean hasType(Node node, String type) {
		return node.getType().equals(type);
	}
	
	public static boolean hasType(Process process, String type) {
		return process.getType().equals(type);
	}
	
	public static boolean hasType(Edge edge, String type) {
		return edge.getType().equals(type);
	}
	
	public static boolean isActivity(Node node) {
		String processType = node.getProcess().getType();
		
		switch (processType) {
			case ProcessTypes.PETRI : return hasType(node, PETRI_TRANSITION) && TextUtils.isActivityLabel(node.getLabel());
			case ProcessTypes.BPMN : return hasType(node, BPMN_ACTIVITY);// || checkEvent(node);
			case ProcessTypes.EPC : return hasType(node, EPC_ACTIVITY);
		}
		
		return false;
	}

	public static boolean isGateway(Node node) {
		String processType = node.getProcess().getType();
		
		switch (processType) {
			case ProcessTypes.PETRI : return checkPetriGateway(node);
			case ProcessTypes.BPMN : return hasType(node, BPMN_EXCLUSIVE_GATEWAY) || hasType(node, BPMN_PARALLEL_GATEWAY) || hasType(node, BPMN_INCLUSIVE_GATEWAY);
			case ProcessTypes.EPC : return hasType(node, EPC_AND) || hasType(node, EPC_XOR) || hasType(node, EPC_OR);
		}
		
		return false;
	}

	private static boolean checkPetriGateway(Node node) {
		if (hasType(node, PETRI_PLACE) && (node.getProcess().getEdgesWithSource(node).size() > 1 || node.getProcess().getEdgesWithTarget(node).size() > 1)) {
			return true;
		}
		
		if (hasType(node, PETRI_TRANSITION) && !isActivity(node) && (node.getProcess().getEdgesWithSource(node).size() > 1 || node.getProcess().getEdgesWithTarget(node).size() > 1)) {
			return true;
		}
		return false;
	}

	public static String getParallelGatewayType(Process process) {
		String processType = process.getType();
		
		switch (processType) {
			case ProcessTypes.PETRI : return PETRI_TRANSITION;
			case ProcessTypes.BPMN : return BPMN_PARALLEL_GATEWAY;
			default : return EPC_AND;
		}
	}
}
