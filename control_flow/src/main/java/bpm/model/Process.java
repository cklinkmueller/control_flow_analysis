package bpm.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class Process {
	private long id;
	private String name;
	private String type;
	
	private List<Node> nodes;
	private List<Edge> edges;
	private Map<Node, Set<Edge>> edgesWithSource;
	private Map<Node, Set<Edge>> edgesWithTarget;
	
	public Process() {
		this("");
	}
	
	public Process(String name) {
		this.name = name;
		
		this.nodes = new LinkedList<Node>();
		this.edges = new LinkedList<Edge>();
		this.edgesWithSource = new HashMap<Node, Set<Edge>>();
		this.edgesWithTarget = new HashMap<Node, Set<Edge>>();
	}
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void addNode(Node node) {
		this.nodes.add(node);
		this.edgesWithSource.put(node, new HashSet<Edge>());
		this.edgesWithTarget.put(node, new HashSet<Edge>());
	}
	
	public void removeNode(Node node) {
		this.nodes.remove(node);
		this.edges.removeAll(this.edgesWithSource.remove(node));
		this.edges.removeAll(this.edgesWithTarget.remove(node));
	}
	
	public int nodesSize() {
		return this.nodes.size();
	}
	
	public Collection<Node> getNodes() {
		return this.nodes;
	}
	
	public void addEdge(Edge edge) {
		this.edges.add(edge);
		
		if (!this.nodes.contains(edge.getSource())) {
			this.nodes.add(edge.getSource());
		}
		
		if (!this.nodes.contains(edge.getTarget())) {
			this.nodes.add(edge.getTarget());
		}
		
		this.edgesWithSource.get(edge.getSource()).add(edge);
		this.edgesWithTarget.get(edge.getTarget()).add(edge);
	}
	
	public void removeEdge(Edge edge) {
		this.edges.remove(edge);
		
		this.edgesWithSource.get(edge.getSource()).remove(edge);
		this.edgesWithTarget.get(edge.getTarget()).remove(edge);
	}
	
	public int edgesSize() {
		return this.edges.size();
	}
	
	public Iterable<Edge> getEdges() {
		return this.edges;
	}
	
	public Collection<Edge> getEdgesWithSource(Node node) {
		return this.edgesWithSource.get(node);
	}
	
	public Collection<Node> getSourceNodes(Node node) {
		HashSet<Node> sources = new HashSet<Node>();
		for (Edge e : this.getEdgesWithTarget(node)) {
			sources.add(e.getSource());
		}
		return sources;
	}
	
	public Collection<Edge> getEdgesWithTarget(Node node) {
		return this.edgesWithTarget.get(node);
	}
	
	public Collection<Node> getTargetNodes(Node node) {
		HashSet<Node> targets = new HashSet<Node>();
		for (Edge e : this.getEdgesWithSource(node)) {
			targets.add(e.getTarget());
		}
		return targets;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void clearNodes() {
		this.edgesWithSource.clear();
		this.edgesWithTarget.clear();
		this.edges.clear();
		this.nodes.clear();		
	}
	
	public boolean equals(Object o) {
		if (o instanceof Process) {
			return this.getId() == ((Process)o).getId();
		}
		
		return false;
	}
	
	public int hashCode() {
		return Long.valueOf(this.getId()).hashCode();
	}
}
