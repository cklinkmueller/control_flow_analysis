package bpm.model;

import java.util.LinkedList;
import java.util.List;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class ProcessUtilities {
	public static List<Node> getActivities(Process process) {
		LinkedList<Node> activities = new LinkedList<Node>();
		for (Node node : process.getNodes()) {
			if (NodeTypes.isActivity(node)) {
				activities.add(node);
			}
		}
		return activities;
	}
	
	public static int countActivities(Process process) {
		int number = 0;
		
		for (Node node : process.getNodes()) {
			if (NodeTypes.isActivity(node)) {
				number++;
			}
		}
		 
		return number;
	}
}
