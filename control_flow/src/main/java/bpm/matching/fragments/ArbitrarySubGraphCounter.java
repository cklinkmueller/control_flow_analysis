package bpm.matching.fragments;

import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

import bpm.matching.properties.graph.NeighborhoodUtil;
import bpm.model.Node;
import bpm.model.NodeTypes;
import bpm.model.Process;
import bpm.model.ProcessUtilities;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class ArbitrarySubGraphCounter {
	
	public BigInteger countSubgraphs(Collection<Process> processes) {
		BigInteger sum = BigInteger.ZERO;
		
		for (Process p : processes) {
			sum = sum.add(this.countSubgraphs(p));
		}
		
		return sum;
	}
 	
	public BigInteger countSubgraphs(Process process) {
		Collection<Node> activities = ProcessUtilities.getActivities(process);
		NeighborMatrix matrix = new NeighborMatrix(process, activities);
		
		LinkedList<ActivityCluster> clusters = new LinkedList<ActivityCluster>();
		clusters.add(new ActivityCluster(activities));
		BigInteger sum = BigInteger.ONE.add(BigInteger.valueOf(activities.size()));
		
		while (clusters.size() > 0) {
			clusters = this.getNewClusters(clusters, matrix);
			sum = sum.add(BigInteger.valueOf(clusters.size()));
		}
		
		
		return sum;
	}
	
	
	private LinkedList<ActivityCluster> getNewClusters(LinkedList<ActivityCluster> clusters, NeighborMatrix matrix) {
		LinkedList<ActivityCluster> newclusters = new LinkedList<ActivityCluster>();
		for (ActivityCluster oldCluster : clusters) {
			if (oldCluster.getActivities().size() > 2) {
				LinkedList<ActivityCluster> subClusters = this.getSubClusters(oldCluster, matrix);
				
				for (ActivityCluster sub : subClusters) {
					if (!containedIn(sub, newclusters)) {
						newclusters.add(sub);
					}
				}
			}
		}
		
		return newclusters;
	}


	private LinkedList<ActivityCluster> getSubClusters(ActivityCluster oldCluster, NeighborMatrix matrix) {
		LinkedList<ActivityCluster> subs = new LinkedList<ActivityCluster>();
		
		for (Node ac : oldCluster.getActivities()) {
			ActivityCluster cl = new ActivityCluster();
			for (Node a : oldCluster.getActivities()) {
				if (a != ac) {
					cl.addActivity(a);
				}
			}
			subs.add(cl);
		}
		
		LinkedList<ActivityCluster> newsubs = new LinkedList<ActivityCluster>();
		for (ActivityCluster sub : subs) {
			if (isSubgraph(sub, matrix)) {
				newsubs.add(sub);
			}
		}
		
		return newsubs;
	}


	private boolean isSubgraph(ActivityCluster sub, NeighborMatrix matrix) {
		LinkedList<Node> subGraph = new LinkedList<Node>();
		subGraph.add(sub.getActivities().getFirst());
		
		while (sub.getActivities().size() != subGraph.size()) {
			Node next = null;
			for (Node ac : sub.getActivities()) {
				if (!subGraph.contains(ac)) {
					for (Node subac : subGraph) {
						if (matrix.areNeighbors(ac, subac)) {
							next = ac;
							break;
						}
					}
					if (next != null) {
						break;
					}
				}
			}
			
			if (next == null) {
				return false;
			}
			else {
				subGraph.add(next);
			}
		}
		
		return true;
	}


	private boolean containedIn(ActivityCluster sub, LinkedList<ActivityCluster> newclusters) {
		for (ActivityCluster cl : newclusters) {
			if (sub.getActivities().size() == cl.activities.size()) {
				boolean contain = true;
				for (Node ac : cl.getActivities()) {
					if (!sub.getActivities().contains(ac)) {
						contain = false;
						break;
					}
				}
				if (contain) {
					return true;
				}
			}
		}
		return false;
	}


	private class NeighborMatrix {
		private HashMap<Long, HashMap<Long, Boolean>> neighbors;
		
		public NeighborMatrix(Process process, Collection<Node> nodes) {
			this.neighbors = new HashMap<Long, HashMap<Long, Boolean>>();
			
			for (Node node : nodes) {
				HashMap<Long, Boolean> map = new HashMap<Long, Boolean>();
				this.neighbors.put(node.getId(), map);
				for (Node n2 : nodes) {
					map.put(n2.getId(), false);
				}
			}
			
			for (Node node : nodes) {
				Set<Node> neighs = NeighborhoodUtil.determineDownstreamNeighborhood(node);
				for (Node neigh : neighs) {
					if (NodeTypes.isActivity(neigh)) {
						this.neighbors.get(node.getId()).put(neigh.getId(), true);
						this.neighbors.get(neigh.getId()).put(node.getId(), true);
					}
				}
			}
		}
		
		public boolean areNeighbors(Node ac1, Node ac2) {
			return this.neighbors.get(ac1.getId()).get(ac2.getId());
		}
	}
	
	private class ActivityCluster {
		private LinkedList<Node> activities;
		
		public ActivityCluster() {
			this.activities = new LinkedList<Node>();
		}
		
		public ActivityCluster(Collection<Node> activities) {
			this();
			
			for (Node ac : activities) {
				this.addActivity(ac);
			}
		}
		
		public void addActivity(Node activity) {
			this.activities.add(activity);
		}
		
		public LinkedList<Node> getActivities() {
			return this.activities;
		}
	}
}
