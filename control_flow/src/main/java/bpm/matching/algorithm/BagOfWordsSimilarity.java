package bpm.matching.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import bpm.text.similarity.WordComparer;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class BagOfWordsSimilarity {
	private WordComparer comparer;
	private boolean pruning;
	
	public BagOfWordsSimilarity(WordComparer comparer, boolean pruning) {
		this.comparer = comparer;
		this.pruning = pruning;
	}
	
	public double calculateSimilarity(ActivityPair ap) {
		double[] sims1 = new double[ap.getBagOfWords1().size()];
		double[] sims2 = new double[ap.getBagOfWords2().size()];
		
		for (int i1 = 0; i1 < ap.getBagOfWords1().size(); i1++) {
			String w1 = ap.getBagOfWords1().get(i1);
			for (int i2 = 0; i2 < ap.getBagOfWords2().size(); i2++) {
				String w2 = ap.getBagOfWords2().get(i2);
				double sim = this.comparer.getSimilarity(w1, w2);
				sims1[i1] = Math.max(sims1[i1], sim);
				sims2[i2] = Math.max(sims2[i2], sim);
			}
		}
		
		if (this.pruning) {
			if (sims1.length > sims2.length) {
				sims1 = this.prune(sims1,sims2.length);
			}
			else if (sims2.length > sims1.length) {
				sims2 = this.prune(sims2,sims1.length);
			}
		}
		
		double sum = 0;
		for (int a = 0; a < sims1.length; a++) {
			sum += sims1[a];
		}
		for (int a = 0; a < sims2.length; a++) {
			sum += sims2[a];
		}
		
		return sum / (sims1.length + sims2.length);
	}

	private double[] prune(double[] set, int size) {
		List<Double> list = new ArrayList<Double>();
		for (double v : set) {
			list.add(v);
		}
		
		Collections.sort(list);
		list = list.subList(list.size() - size, list.size());
		
		double[] sub = new double[size];
		for (int a = 0; a < size; a++) {
			sub[a] = list.get(a);
		}
		return sub;
	}

	public void setPruning(boolean pruning) {
		this.pruning = pruning;
	}
	
	public boolean isPruning() {
		return this.pruning;
	}

	public WordComparer getComparer() {
		return comparer;
	}

	public void setComparer(WordComparer comparer) {
		this.comparer = comparer;
	}
}
