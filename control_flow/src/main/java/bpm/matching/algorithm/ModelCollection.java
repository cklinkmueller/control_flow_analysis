package bpm.matching.algorithm;

import java.util.LinkedList;
import java.util.List;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class ModelCollection {
	private double score;
	private LinkedList<ModelPair> modelPairs;
	private double threshold;
	private String key;
	private double minimumThreshold;
	private double maximumThreshold;
	private BOT bot;
	
	public ModelCollection() {
		this.modelPairs = new LinkedList<ModelPair>();
	}
	
	public BOT getBot() {
		return bot;
	}

	public void setBot(BOT bot) {
		this.bot = bot;
	}

	public double getScore() {
		return this.score;
	}
	
	public void setScore(double score) {
		this.score = score;
	}
	
	public double getThreshold() {
		return this.threshold;
	}
	
	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}
	
	public double getMinimumThreshold() {
		return this.minimumThreshold;
	}
	
	public void setMinimumThreshold(double minimumThreshold) {
		this.minimumThreshold = minimumThreshold;
	}
	
	public double getMaximumThreshold() {
		return this.maximumThreshold;
	}
	
	public void setMaximumThreshold(double maximumThreshold) {
		this.maximumThreshold = maximumThreshold;
	}
	
	public String getKey() {
		return this.key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public void addModelPair(ModelPair modelPair) {
		this.modelPairs.add(modelPair);
	}
	
	public LinkedList<ModelPair> getModelPairs() {
		return this.modelPairs;
	}

	public static ModelCollection getCollection(List<ModelPair> pairs) {
		ModelCollection collection = new ModelCollection();
				
		for (ModelPair mp : pairs) {
			ModelPair cmp = new ModelPair(mp.getProcess1(), mp.getProcess2());
			
			for (ActivityPair ap : mp.getActivityPairs()) {
				ActivityPair cap = new ActivityPair(cmp, ap.getActivity1(), ap.getActivity2());
				cap.setGoldCorresponding(ap.isGoldCorresponding());
				cap.setBagOfWords1(ap.getBagOfWords1());
				cap.setBagOfWords2(ap.getBagOfWords2());
				cap.setStartDistance1(ap.getStartDistance1());
				cap.setStartDistance2(ap.getStartDistance2());
				cmp.getActivityPairs().add(cap);
			}
			
			collection.addModelPair(cmp);
		}
		
		return collection;
	}

	public void determineSimilarities() {
		this.bot.matchModelPairs(this.modelPairs);
	}
}
