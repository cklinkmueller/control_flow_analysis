package bpm.matching.algorithm;

import java.util.HashSet;
import java.util.List;

import bpm.model.Node;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class BOT {
	private BagOfWordsSimilarity similarity;
	
	protected BOT() {}
	
	public BOT(BagOfWordsSimilarity similarity) {
		this.similarity = similarity;
	}
	
	public void matchModelPairs(List<ModelPair> pairs) {		
		for (ModelPair mp : pairs) {
			HashSet<Node> equals = new HashSet<Node>();
			for (ActivityPair ap : mp.getActivityPairs()) {
				double sim = 0;
				if (ap.getActivity1().getLabel().equals(ap.getActivity2().getLabel())) {
					sim = 1;
					equals.add(ap.getActivity1());
					equals.add(ap.getActivity2());
				}
				this.setEqualLabelSimilarity(ap, sim);
			}
			
			for (ActivityPair ap : mp.getActivityPairs()) {
				if (!equals.contains(ap.getActivity1()) && !equals.contains(ap.getActivity2())) {
					this.setSimilarity(ap);
				}
			}
		}
	}

	private void setEqualLabelSimilarity(ActivityPair ap, double sim) {
		ap.setSimilarity(sim);
	}

	protected void setSimilarity(ActivityPair ap) {
		ap.setSimilarity(this.similarity.calculateSimilarity(ap)); 
	}
}
