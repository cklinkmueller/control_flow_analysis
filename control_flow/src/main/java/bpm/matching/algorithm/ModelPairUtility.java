package bpm.matching.algorithm;

import java.util.LinkedList;
import java.util.List;

import bpm.matching.properties.graph.StartNodeDistanceProperty;
import bpm.model.Alignment;
import bpm.model.Node;
import bpm.model.ProcessUtilities;
import bpm.text.TextUtils;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class ModelPairUtility {
	private StartNodeDistanceProperty property = null;
	
	public ModelPairUtility(StartNodeDistanceProperty property) {
		this.property = property;
	}
	
	public List<ModelPair> createModelPairs(List<Alignment> alignments) {
		LinkedList<ModelPair> modelPairs = new LinkedList<ModelPair>();
		
		for (Alignment alignment : alignments) {
			ModelPair mp = new ModelPair(alignment.getProcess1(), alignment.getProcess2());
			
			double max1 = this.property.getMaximum(alignment.getProcess1());
			double max2 = this.property.getMaximum(alignment.getProcess2());
			
			for (Node ac1 : ProcessUtilities.getActivities(alignment.getProcess1())) {
				for (Node ac2 : ProcessUtilities.getActivities(alignment.getProcess2())) {
					ActivityPair ap = new ActivityPair(mp, ac1, ac2);
					ap.setStartDistance1(max1 == 0 ? 0 : this.property.getAttributeValue(ac1) / max1);
					ap.setStartDistance2(max2 == 0 ? 0 : this.property.getAttributeValue(ac2) / max2);
					ap.setBagOfWords1(TextUtils.tokenizeAndRemoveStopWords(ac1.getLabel()));
					ap.setBagOfWords2(TextUtils.tokenizeAndRemoveStopWords(ac2.getLabel()));
					ap.setGoldCorresponding(alignment.areCorresponding(ac1, ac2));
					mp.getActivityPairs().add(ap);
				}
			}
			
			modelPairs.add(mp);
		}
		
		return modelPairs;
	}
}
