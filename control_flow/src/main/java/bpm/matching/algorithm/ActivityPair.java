package bpm.matching.algorithm;

import java.util.List;

import bpm.model.Node;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class ActivityPair {
	private Node activity1;
	private Node activity2;
	private double startDistance1;
	private double startDistance2;
	private double similarity;
	private List<String> bagOfWords1;
	private List<String> bagOfWords2;
	private boolean corresponding;
	private boolean goldCorresponding;
	private ModelPair mp;
	
	public ActivityPair(ModelPair mp, Node activity1, Node activity2) {
		this.activity1 = activity1;
		this.activity2 = activity2;
		this.mp = mp;
	}

	public Node getActivity1() {
		return activity1;
	}

	public void setActivity1(Node activity1) {
		this.activity1 = activity1;
	}

	public Node getActivity2() {
		return activity2;
	}

	public void setActivity2(Node activity2) {
		this.activity2 = activity2;
	}

	public double getStartDistance1() {
		return startDistance1;
	}

	public void setStartDistance1(double startDistance1) {
		this.startDistance1 = startDistance1;
	}

	public double getStartDistance2() {
		return startDistance2;
	}

	public void setStartDistance2(double startDistance2) {
		this.startDistance2 = startDistance2;
	}

	public double getSimilarity() {
		return this.similarity;
	}

	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}

	public List<String> getBagOfWords1() {
		return bagOfWords1;
	}

	public void setBagOfWords1(List<String> bagOfWords1) {
		this.bagOfWords1 = bagOfWords1;
	}

	public List<String> getBagOfWords2() {
		return bagOfWords2;
	}

	public void setBagOfWords2(List<String> bagOfWords2) {
		this.bagOfWords2 = bagOfWords2;
	}

	public boolean isCorresponding() {
		return corresponding;
	}

	public void setCorresponding(boolean corresponding) {
		this.corresponding = corresponding;
	}

	public boolean isGoldCorresponding() {
		return goldCorresponding;
	}

	public void setGoldCorresponding(boolean goldCorresponding) {
		this.goldCorresponding = goldCorresponding;
	}

	public ModelPair getModelPair() {
		return this.mp;
	}
}
