package bpm.matching.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import bpm.text.similarity.WordComparer;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class OPBOT {
	private ArrayList<ModelCollection> collections;
	
	private WordComparer lin;
	private WordComparer lev;
	private WordComparer cco;
	
	public OPBOT(WordComparer lin, WordComparer lev, WordComparer cco) {
		this.collections = new ArrayList<ModelCollection>();
		
		this.lin = lin;
		this.lev = lev;
		this.cco = cco;
	}
	
	public List<ModelPair> matchModelPairs(List<ModelPair> pairs) {
		this.preprocess(pairs);
		this.search();
		ModelCollection result = this.combine();
		
		return result.getModelPairs();
	}

	private void preprocess(List<ModelPair> pairs) {
		double min = 0.7;
		double max = 1.0;
		boolean prune = false;		
		this.addCollection(pairs, this.cco, prune, min, max);
		
		prune = true;		
		this.addCollection(pairs, this.cco, prune, min, max);
		
		min = 0.6;
		prune = false;		
		this.addCollection(pairs, this.lev, prune, min, max);
		
		prune = true;		
		this.addCollection(pairs, this.lev, prune, min, max);
		
		prune = false;		
		this.addCollection(pairs, this.lin, prune, min, max);
		
		prune = true;		
		this.addCollection(pairs, this.lin, prune, min, max);
	}

	private void addCollection(List<ModelPair> pairs, WordComparer comp, boolean prune, double min, double max) {
		ModelCollection collection = ModelCollection.getCollection(pairs);
		BagOfWordsSimilarity sim = new BagOfWordsSimilarity(comp, prune);
		BOT bot = new BOT(sim);
		collection.setBot(bot);
		collection.setMinimumThreshold(min);
		collection.setMaximumThreshold(max);		
		this.collections.add(collection);
	}
	
	double threshold = 0;
	
	private void search() {
		for (ModelCollection collection : this.collections) {
			// determine similarities for activity pairs
			collection.determineSimilarities();
			
			// determine scores per threshold
			this.determineThreshold(collection);
		}
		
		// Sort Collections by Score
		Collections.sort(this.collections, new Comparator<ModelCollection>() {
			public int compare(ModelCollection o1, ModelCollection o2) {
				return Double.compare(o2.getScore(), o1.getScore());
			}			
		});
	}

	private void determineThreshold(ModelCollection collection) {
		// determine possible thresholds as sorted list
		List<Double> thresholds = this.determineThresholds(collection.getModelPairs());
		
		int min = 0;
		while (min < thresholds.size() && thresholds.get(min) < collection.getMinimumThreshold()) {
			min++;
		}
		
		int max = min;
		while (max < thresholds.size() && thresholds.get(max) <= collection.getMaximumThreshold()) {
			max++;
		}
		
		HashMap<Integer, Double> threshScores = new HashMap<Integer, Double>();
		for (int a = min; a < max; a++) {
			double thresh = thresholds.get(a);
			double score = this.determineScore(collection.getModelPairs(), thresh);
			threshScores.put(a, score);
		}
		
		double thresh = 0;
		double maxScore = 0;
		
		for (int a = min; a < max; a++) {
			if (threshScores.get(a) > maxScore) {
				maxScore = threshScores.get(a);
				thresh = thresholds.get(a);
			}
		}
		
		collection.setThreshold(thresh);
		collection.setScore(maxScore);
	}
	
	private double determineScore(LinkedList<ModelPair> modelPairs, double thresh) {
		double score = 0;
		for (ModelPair mp : modelPairs) {
			double sum = this.determineScore(mp, thresh);
			score += sum;
		}
		
		score /= modelPairs.size();
		return score;
	}

	private double determineScore(ModelPair mp, double thresh) {
		int num = 0;
		double sum = 0;
		
		List<ActivityPair> correspondences = this.getCorrespondences(mp, thresh);
		for (int a = 0; a < correspondences.size() - 1; a++) {
			ActivityPair c1 = correspondences.get(a);
			for (int b = a + 1; b < correspondences.size(); b++) {
				ActivityPair c2 = correspondences.get(b);
				sum += (c1.getStartDistance1() - c2.getStartDistance1()) * (c1.getStartDistance2() - c2.getStartDistance2()) >= 0 ? 1 : 0;
				num++;
			}
		}
		
		sum = num == 0 ? 0 : sum / num;
		return sum;
	}

	private List<ActivityPair> getCorrespondences(ModelPair mp, double thresh) {
		ArrayList<ActivityPair> corrs = new ArrayList<ActivityPair>();
		
		for (ActivityPair ap : mp.getActivityPairs()) {
			if (ap.getSimilarity() >= thresh) {
				corrs.add(ap);
			}
		}
		
		return corrs;
	}

	public List<Double> determineThresholds(List<ModelPair> pairs) {
		HashSet<Double> threshs = new HashSet<Double>();
		
		for (ModelPair p : pairs) {
			for (ActivityPair a : p.getActivityPairs()) {
				if (!threshs.contains(a.getSimilarity())) {
					threshs.add(a.getSimilarity());
				}
			}
		}
		
		LinkedList<Double> thresholds = new LinkedList<Double>(threshs);
		Collections.sort(thresholds);
		
		return thresholds;
	}

	private ModelCollection combine() {
		double thresh = 1d;
		
		for (ModelPair mp : this.collections.get(0).getModelPairs()) {
			for (ActivityPair ap : mp.getActivityPairs()) {
				ActivityPair clone = this.getClone(this.collections.get(1), mp, ap);
			
				if (ap.getSimilarity() >= this.collections.get(0).getThreshold() || clone.getSimilarity() >= this.collections.get(1).getThreshold()) {
					ap.setSimilarity(Math.max(ap.getSimilarity(), clone.getSimilarity()));
					ap.setCorresponding(true);
					Math.min(thresh, ap.getSimilarity());
				}
				else {
					ap.setSimilarity(0);
					ap.setCorresponding(false);
				}
			}
		}
		
		this.collections.get(0).setThreshold(thresh);
		return this.collections.get(0);
	}

	private ActivityPair getClone(ModelCollection collection, ModelPair mp, ActivityPair ap) {
		ModelPair pair = null;
		for (ModelPair cmp : collection.getModelPairs()) {
			if (cmp.getProcess1() == mp.getProcess1() && cmp.getProcess2() == mp.getProcess2()) {
				pair = cmp;
				break;
			}
		}
		
		ActivityPair clone = null;
		for (ActivityPair cap : pair.getActivityPairs()) {
			if (cap.getActivity1() == ap.getActivity1() && cap.getActivity2() == ap.getActivity2()) {
				clone = cap;
				break;
			}
		}
		
		pair.getActivityPairs().remove(clone);
		
		if (pair.getActivityPairs().isEmpty()) {
			collection.getModelPairs().remove(pair);
		}
		
		return clone;
	}	
}
