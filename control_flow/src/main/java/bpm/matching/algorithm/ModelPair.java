package bpm.matching.algorithm;

import java.util.LinkedList;
import java.util.List;

import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class ModelPair {
	private Process process1;
	private Process process2;
	private List<ActivityPair> activityPairs;
	
	public ModelPair(Process process1, Process process2) {
		this.process1 = process1;
		this.process2 = process2;
		this.activityPairs = new LinkedList<ActivityPair>();
	}

	public Process getProcess1() {
		return process1;
	}

	public void setProcess1(Process process1) {
		this.process1 = process1;
	}

	public Process getProcess2() {
		return process2;
	}

	public void setProcess2(Process process2) {
		this.process2 = process2;
	}

	public List<ActivityPair> getActivityPairs() {
		return activityPairs;
	}
}
