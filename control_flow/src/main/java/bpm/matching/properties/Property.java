package bpm.matching.properties;

import java.util.Collection;
import java.util.HashMap;

import bpm.model.Node;
import bpm.model.NodeTypes;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public abstract class Property {
	protected HashMap<Long, Double> values;
	protected HashMap<Long, Double> maxima;
	
	public Property(Collection<Process> processes) {
		this.values = new HashMap<Long, Double>();
		this.maxima = new HashMap<Long, Double>();
		this.initialize(processes);
	}
	
	protected void initialize(Collection<Process> processes) {
		for (Process process : processes) {
			this.initialize(process);
		}
	}
	
	protected void initialize(Process process) {
		double max = Double.MIN_VALUE;
		
		for (Node node : process.getNodes()) {
			if (NodeTypes.isActivity(node)) {
				double value = this.determineValue(node);
				max = Math.max(max, value);
				this.values.put(node.getId(), value);
			}
		}
		
		this.maxima.put(process.getId(), max);
	}

	protected abstract double determineValue(Node node);

	public final double getIndicator(Node node1, Node node2) {
		if (!NodeTypes.isActivity(node1) || !NodeTypes.isActivity(node2)) {
			return 0;
		}
		
		double indicator1 = this.getAttributeValue(node1);
		double indicator2 = this.getAttributeValue(node2);
		
		double max1 = this.getMaximum(node1.getProcess());
		double max2 = this.getMaximum(node2.getProcess());
		
		double ratio1 = max1 == 0 ? 0 : indicator1 / max1;
		double ratio2 = max2 == 0 ? 0 : indicator2 / max2;
		
		return 1 - Math.abs(ratio1 - ratio2);
	}

	public final double getMaximum(Process process) {
		return this.maxima.get(process.getId());
	}
	
	public final double getAttributeValue(Node node) {
		return this.values.get(node.getId());	
	}
}
