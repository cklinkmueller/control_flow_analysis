package bpm.matching.properties.exsem;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import org.jbpt.bp.BehaviouralProfile;
import org.jbpt.bp.construct.BPCreatorNet;
import org.jbpt.petri.NetSystem;
import org.jbpt.petri.PetriNet;
import org.jbpt.petri.Place;
import org.jbpt.petri.Transition;

import bpm.model.Edge;
import bpm.model.Node;
import bpm.model.NodeTypes;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

class BehavioralProfileUtils {	
	public static final BehavioralProfileUtils INSTANCE;
	static {
		INSTANCE = new BehavioralProfileUtils();
	}
	
	private HashMap<Process, BehavioralProfile> profiles;
	public BehavioralProfileUtils() {
		this.profiles = new HashMap<Process, BehavioralProfile>();
	}
	
	public BehavioralProfile getProfile(Process process) {
		if (this.profiles.containsKey(process)) {
			return this.profiles.get(process);
		}
		else {
			BehavioralProfile profile = this.determineProfile(process);
			this.profiles.put(process, profile);
			return profile;
		}
	}
	
	private BehavioralProfile determineProfile(Process process) {
		PetriNet net = this.getPetriNet(process);
		BPCreatorNet creator = BPCreatorNet.getInstance();
		NetSystem system = new NetSystem(net);
		
		Collection<org.jbpt.petri.Node> activities = getRelevantTransitions(net);
		
		BehaviouralProfile<NetSystem, org.jbpt.petri.Node> profile = creator.deriveRelationSet(system, activities);
		
		return new BehavioralProfile(profile, activities);
	}
	
	private Collection<org.jbpt.petri.Node> getRelevantTransitions(PetriNet net) {
		LinkedList<org.jbpt.petri.Node> transitions = new LinkedList<org.jbpt.petri.Node>();
		for (org.jbpt.petri.Node node : net.getNodes()) {
			if (node instanceof Transition && NodeTypes.isActivity((Node)node.getTag())) {
				transitions.add((org.jbpt.petri.Node)node);
			}
		}
		return transitions;
	}

	private PetriNet getPetriNet(Process process) {
		PetriNet net = new PetriNet();
		
		HashMap<Node, org.jbpt.petri.Node> map = new HashMap<Node, org.jbpt.petri.Node>();
		for (Node node : process.getNodes()) {
			if (NodeTypes.hasType(node, NodeTypes.PETRI_PLACE)) {
				Place place = new Place();
				place.setId(Long.toString(node.getId()));
				place.setLabel(node.getLabel());
				place.setTag(node);
				map.put(node, place);
				
			}
			else if (NodeTypes.hasType(node, NodeTypes.PETRI_TRANSITION)) {
				Transition transition = new Transition();
				transition.setId(Long.toString(node.getId()));
				transition.setLabel(node.getLabel());
				transition.setTag(node);
				net.addTransition(transition);
				map.put(node, transition);
			}
		}
				
		for (Edge edge : process.getEdges()) {
			org.jbpt.petri.Node s = map.get(edge.getSource());
			org.jbpt.petri.Node t = map.get(edge.getTarget());
			net.addFlow(s, t);
		}
		
		return net;
	}
}
