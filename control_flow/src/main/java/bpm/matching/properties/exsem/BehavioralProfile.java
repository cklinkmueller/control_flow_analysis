package bpm.matching.properties.exsem;

import java.util.Collection;
import java.util.HashMap;

import org.jbpt.bp.BehaviouralProfile;
import org.jbpt.petri.NetSystem;

import bpm.model.Node;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

class BehavioralProfile {
	private HashMap<Long, Integer> order;
	private HashMap<Long, Integer> exclusive;
	private HashMap<Long, Integer> interleaving;
	private HashMap<Long, Integer> inverse;
	
	public BehavioralProfile(BehaviouralProfile<NetSystem, org.jbpt.petri.Node> profile, Collection<org.jbpt.petri.Node> activities) {
		this.initialize(activities);
		
		for (org.jbpt.petri.Node t1 : activities) {
			for (org.jbpt.petri.Node t2 : activities) {
				if (t1 != t2) {
					if (profile.areExclusive(t1, t2)) {
						this.increase(this.exclusive, t1);
					}
					else if (profile.areInterleaving(t1, t2)) {
						this.increase(this.interleaving, t1);
					}
					else if (profile.areInOrder(t1, t2)) {
						this.increase(this.order, t1);
					}
					else if (profile.areInOrder(t2, t1)) {
						this.increase(this.inverse, t1);
					}
				}
			}
		}
	}
	
	private void increase(HashMap<Long, Integer> map, org.jbpt.petri.Node node) {
		long id = ((Node)node.getTag()).getId();
		int val = map.get(id) + 1;
		map.put(id, val);
	}

	private void initialize(Collection<org.jbpt.petri.Node> activities) {
		this.order = new HashMap<Long, Integer>();
		this.interleaving = new HashMap<Long, Integer>();
		this.exclusive = new HashMap<Long, Integer>();
		this.inverse = new HashMap<Long, Integer>();
		
		for (org.jbpt.petri.Node ac : activities) {
			long id = ((Node)ac.getTag()).getId();
			this.order.put(id, 0);
			this.interleaving.put(id, 0);
			this.exclusive.put(id, 0);
			this.inverse.put(id, 0);
		}
	}

	public int getExclusive(Node node) {
		return this.exclusive.get(node.getId());
	}
	
	public int getOrder(Node node) {
		return this.order.get(node.getId());
	}
	
	public int getInterleaving(Node node) {
		return this.interleaving.get(node.getId());
	}
	
	public int getInverse(Node node) {
		return this.inverse.get(node.getId());
	}
}
