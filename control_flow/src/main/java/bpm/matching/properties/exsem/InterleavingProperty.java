package bpm.matching.properties.exsem;

import java.util.Collection;

import bpm.matching.properties.Property;
import bpm.model.Node;
import bpm.model.Process;
import bpm.model.ProcessUtilities;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class InterleavingProperty extends Property {
	public InterleavingProperty(Collection<Process> processes) {
		super(processes);
	}

	@Override
	protected void initialize(Collection<Process> processes) {
		for (Process process : processes) {
			BehavioralProfile bp = BehavioralProfileUtils.INSTANCE.getProfile(process);

			double max = 0;
			for (Node n : ProcessUtilities.getActivities(process)) {
				int v = bp.getInterleaving(n);
				max = Double.max(max, v);
				this.values.put(n.getId(), (double)v);
			}
			
			this.maxima.put(process.getId(), max);
		}
	}
	
	@Override
	protected double determineValue(Node node) {
		return 0;
	}

}
