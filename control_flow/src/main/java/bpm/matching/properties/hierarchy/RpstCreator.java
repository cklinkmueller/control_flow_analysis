package bpm.matching.properties.hierarchy;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import org.jbpt.algo.tree.rpst.IRPSTNode;
import org.jbpt.algo.tree.rpst.RPST;
import org.jbpt.graph.DirectedEdge;
import org.jbpt.graph.DirectedGraph;
import org.jbpt.hypergraph.abs.Vertex;

import bpm.model.Edge;
import bpm.model.Node;
import bpm.model.NodeTypes;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class RpstCreator {
	private long id;
	
	public RpstCreator() {
		this.id = 0;
	}
	
	public RpstActivityNode createRpst(Process process) {
		// transform process to jbpt process
		HashMap<Vertex, Node> nodes = new HashMap<Vertex, Node>();
		DirectedGraph graph = this.transformProcess(process, nodes); 
		
		// add artificial start / end node in case of many start / end nodes
		this.checkBoundaryNodes(graph);
				
		// create rpst
		RPST<DirectedEdge, Vertex> rpst = new RPST<DirectedEdge, Vertex>(graph);
		
		// transform rpst to rpstactivitynode
		RpstActivityNode root = this.transformRpst(rpst, rpst.getRoot(), null, nodes);
		
		return root;
	}
	
	public RpstActivityNode createModifiedRpst(Process process) {
		// transform process to jbpt process
		HashMap<Vertex, Node> nodes = new HashMap<Vertex, Node>();
		DirectedGraph graph = this.transformProcess(process, nodes); 
		
		// add artificial start / end node in case of many start / end nodes
		this.checkBoundaryNodes(graph);
		
		// substitute activities with many incoming / outgoing arcs
		this.checkActivityConnectivity(graph, nodes);
		
		// create rpst
		RPST<DirectedEdge, Vertex> rpst = new RPST<DirectedEdge, Vertex>(graph);
		
		// transform rpst to rpstactivitynode
		RpstActivityNode root = this.transformRpst(rpst, rpst.getRoot(), null, nodes);
		
		return root;
	}

	private RpstActivityNode transformRpst(RPST<DirectedEdge, Vertex> rpst, IRPSTNode<DirectedEdge, Vertex> currentNode, RpstActivityNode parent, HashMap<Vertex, Node> nodes) {
		String type = "";
		
		
		switch (currentNode.getType()) {
			case BOND : type = RpstActivityNode.BOND; break;
			case TRIVIAL : type = RpstActivityNode.TRIVIAL; break;
			case POLYGON : type = RpstActivityNode.POLYGON; break;
			default : type = RpstActivityNode.RIGID; break;
		}
		
		RpstActivityNode rpstNode = new RpstActivityNode(type, parent);
		
		HashSet<Node> fragNodes = new HashSet<Node>(); 
		for (DirectedEdge e : currentNode.getFragment()) {
			//Node n = null;
			if (nodes.containsKey(e.getSource())) {// && NodeTypes.isActivity((n = nodes.get(e.getSource())))) {
				fragNodes.add(nodes.get(e.getSource()));
			}
			if (nodes.containsKey(e.getTarget())) {//&& NodeTypes.isActivity((n = nodes.get(e.getTarget())))) {
				fragNodes.add(nodes.get(e.getTarget()));
			}			
		}
		rpstNode.addNodes(fragNodes);
		
		//LinkedList<Node> trivials = new LinkedList<Node>();
		
		for (IRPSTNode<DirectedEdge, Vertex> childNode : rpst.getChildren(currentNode)) {
			RpstActivityNode child = this.transformRpst(rpst, childNode, rpstNode, nodes);
			rpstNode.addDecendant(child);
			/*if (child.getType().equals(RpstActivityNode.TRIVIAL)) {
				Node n = null;
				if (child.getNodes().size() == 1 && !trivials.contains(n = child.getNodes().get(0))) {
					trivials.add(n);
					rpstNode.addDecendant(child);
				}
			}
			else {
				if (!child.getDecendants().isEmpty()) {
					rpstNode.addDecendant(child);
				}
			}*/
		}
		
		return rpstNode;
	}

	private void checkActivityConnectivity(DirectedGraph graph, HashMap<Vertex, Node> nodes) {
		LinkedList<Vertex> gateways = new LinkedList<Vertex>();
		for (Vertex v : nodes.keySet()) {
			if (NodeTypes.isActivity(nodes.get(v))) {
				if (graph.getDirectPredecessors(v).size() > 1 || graph.getDirectSuccessors(v).size() > 1) {
					gateways.add(v);
				}
			}
		}
		
		for (Vertex g : gateways) {
			Collection<Vertex> sources = graph.getDirectPredecessors(g);
			if (sources.size() > 1) {
				Vertex v = this.createVertex(graph);
				
				for (Vertex s : sources) {
					graph.removeEdge(graph.getDirectedEdge(s, g));
					graph.addEdge(s, v);
				}
				
				graph.addEdge(v, g);
			}
			
			Collection<Vertex> targets = graph.getDirectSuccessors(g);
			if (targets.size() > 1) {
				Vertex v = this.createVertex(graph);
				
				for (Vertex t : targets) {
					graph.removeEdge(graph.getDirectedEdge(g, t));
					graph.addEdge(v, t);
				}
				
				graph.addEdge(v, g);
			}
		}
	}

	private void checkBoundaryNodes(DirectedGraph graph) {
		LinkedList<Vertex> sources = new LinkedList<Vertex>();
		LinkedList<Vertex> targets = new LinkedList<Vertex>();
		this.getSourcesAndTargets(graph, sources, targets);
		
		this.addArtificialStartNode(graph, sources);
		this.addArtificialEndNode(graph, targets);		
	}

	private void addArtificialEndNode(DirectedGraph graph, LinkedList<Vertex> targets) {
		if (targets.size() > 1) {
			Vertex target = this.createVertex(graph);
			for (Vertex source : targets) {
				graph.addEdge(source, target);
			}
		}
	}

	private void addArtificialStartNode(DirectedGraph graph, LinkedList<Vertex> sources) {
		if (sources.size() > 1) {
			Vertex source = this.createVertex(graph);
			for (Vertex target : sources) {
				graph.addEdge(source, target);
			}			
		}
	}

	private void getSourcesAndTargets(DirectedGraph graph, LinkedList<Vertex> sources, LinkedList<Vertex> targets) {
		for (Vertex v : graph.getVertices()) {
			if (graph.getDirectPredecessors(v).isEmpty()) {
				sources.add(v);
			}
			if (graph.getDirectSuccessors(v).isEmpty()) {
				targets.add(v);
			}
		}
	}

	private DirectedGraph transformProcess(Process process, HashMap<Vertex, Node> nodes) {
		HashMap<Node, Vertex> vertecies = new HashMap<Node, Vertex>();
		
		DirectedGraph graph = new DirectedGraph();
		
		for (Node node : process.getNodes()) {
			Vertex v = this.createVertex(graph);
			vertecies.put(node, v);
			nodes.put(v, node);
		}
		
		for (Edge edge : process.getEdges()) {
			Vertex s = vertecies.get(edge.getSource());
			Vertex t = vertecies.get(edge.getTarget());
			graph.addEdge(s, t);
		}
		
		return graph;
	}
	
	private Vertex createVertex(DirectedGraph graph) {
		Vertex v = new Vertex();
		v.setId(Long.toString(id++));
		graph.addVertex(v);
		return v;
	}
}
