package bpm.matching.properties.hierarchy;

import java.util.Collection;
import java.util.LinkedList;

import bpm.matching.properties.Property;
import bpm.model.Node;
import bpm.model.NodeTypes;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class RpstNeighborhoodProperty extends Property {
	private RpstActivityNode root;
	
	public RpstNeighborhoodProperty(Collection<Process> processes) {
		super(processes);
	}
	
	@Override
	protected void initialize(Process process) {
		RpstCreator creator = new RpstCreator();
		this.root = creator.createRpst(process);
		
		double max = 0;
		for (Node node : process.getNodes()) {
			if (NodeTypes.isActivity(node)) {
				double val = this.determineValue(node);
				this.values.put(node.getId(), val);
				max = Math.max(max, val);
			}
		}
		
		this.maxima.put(process.getId(), max);
	}

	@Override
	protected double determineValue(Node node) {
		LinkedList<RpstActivityNode> rnodes = new LinkedList<RpstActivityNode>();
		rnodes.add(this.root);
	
		double val = 0;
		while (rnodes.size() != 0) {
			LinkedList<RpstActivityNode> newnodes = new LinkedList<RpstActivityNode>();
			
			for (RpstActivityNode rpstnode : rnodes) {
				for (RpstActivityNode n : rpstnode.getDecendants()) {
					if (n.getNodes().contains(node)) {
						if (n.getType().equals(RpstActivityNode.TRIVIAL)) {
							for (Node no : n.getAncestor().getNodes()) {
								if (NodeTypes.isActivity(no)) {
									val++;
								}
							}
						}
						else {
							newnodes.add(n);
						}
					}
				}
			}
			
			rnodes = newnodes;
		}
		
		return val;
	}

}
