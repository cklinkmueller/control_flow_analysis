package bpm.matching.properties.hierarchy;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import bpm.model.Node;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class RpstActivityNode {
	public static final String TRIVIAL = "trivial";
	public static final String BOND = "bond";
	public static final String RIGID = "rigid";
	public static final String POLYGON = "polygon";
	
	private String type;
	private List<Node> nodes;
	private List<RpstActivityNode> decendants;
	private RpstActivityNode ancestor;
	
	public RpstActivityNode(String type, RpstActivityNode ancestor) {
		this.type = type;
		this.ancestor = ancestor;
		
		this.nodes = new LinkedList<Node>();
		this.decendants = new LinkedList<RpstActivityNode>();
	}
	
	public RpstActivityNode getAncestor() {
		return this.ancestor;
	}
	
	public void setAncestor(RpstActivityNode ancestor) {
		this.ancestor = ancestor;
	}
	
	public String getType() {
		return this.type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void addDecendant(RpstActivityNode decendant) {
		if (decendant != null && !this.decendants.contains(decendant)) {
			this.decendants.add(decendant);
		}
	}
	
	public void removeDecendant(RpstActivityNode decendant) {
		if (decendant != null) {
			this.decendants.remove(decendant);
		}
	}
	
	public List<RpstActivityNode> getDecendants() {
		return this.decendants;
	}
	
	public void addNode(Node node) {
		if (node != null && !this.nodes.contains(node)) {
			this.nodes.add(node);
		}
	}

	public void addNodes(Collection<Node> fragNodes) {
		this.nodes.addAll(fragNodes);
	}
	
	public void removeNode(Node node) {
		if (node != null) {
			this.nodes.remove(node);
		}
	}
	
	public List<Node> getNodes() {
		return this.nodes;
	}
}
