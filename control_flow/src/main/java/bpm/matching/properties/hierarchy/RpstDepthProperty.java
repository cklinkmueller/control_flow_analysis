package bpm.matching.properties.hierarchy;

import java.util.Collection;
import java.util.LinkedList;

import bpm.matching.properties.Property;
import bpm.model.Node;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class RpstDepthProperty extends Property {
	
	public RpstDepthProperty(Collection<Process> processes) {
		super(processes);
	}
	
	@Override
	protected void initialize(Process process) {
		RpstCreator creator = new RpstCreator();
		RpstActivityNode root = creator.createRpst(process);
		
		LinkedList<RpstActivityNode> nodes = new LinkedList<RpstActivityNode>();
		nodes.add(root);
		double depth = 0;
				
		while (!nodes.isEmpty()) {
			LinkedList<RpstActivityNode> children = new LinkedList<RpstActivityNode>();
			
			for (RpstActivityNode n : nodes) {
				if (n.getType().equals(RpstActivityNode.TRIVIAL)) {
					for (Node ac : n.getNodes()) {
						this.values.put(ac.getId(), depth);
					}
				}
				else {
					children.addAll(n.getDecendants());
				}
			}
			
			depth++;
			nodes = children;
		}
		
		this.maxima.put(process.getId(), depth - 1);
	}

	@Override
	protected double determineValue(Node node) {
		return 0;
	}
}
