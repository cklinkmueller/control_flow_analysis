package bpm.matching.properties.graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import bpm.model.Edge;
import bpm.model.Node;
import bpm.model.NodeTypes;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class ActivityGraphTransformer {
	/*protected HashSet<Long> ids;
	
	public ActivityGraphTransformer() {
		ids = new HashSet<Long>();
		ids.add(7792l);
		ids.add(5613l);
		ids.add(16265l);
		ids.add(7591l);
		ids.add(8626l);
		ids.add(16375l);
		ids.add(7229l);
	}*/
	
	public Process transformActivityGraph(Process process) {
		Process activityProcess = new Process();
		activityProcess.setName(process.getName());
		
		HashMap<Node, Node> nodeMap = new HashMap<Node, Node>();
		for (Node node : process.getNodes()) {
			if (NodeTypes.isActivity(node)) {
				Node ac = new Node();
				ac.setId(node.getId());
				ac.setLabel(node.getLabel());
				ac.setType(node.getType());
				ac.setHeight(node.getHeight());
				ac.setWidth(node.getWidth());
				ac.setX(node.getX());
				ac.setY(node.getY());
				ac.setProcess(activityProcess);
				activityProcess.addNode(ac);
				
				nodeMap.put(node, ac);
			}
		}
		
		for (Node node : process.getNodes()) {
			if (NodeTypes.isActivity(node)) {
				List<Node> nodes = this.findTargetActivities(node);
				for (Node target : nodes) {
					Edge edge = new Edge();
					edge.setSource(nodeMap.get(node));
					edge.setTarget(nodeMap.get(target));
					edge.setProcess(activityProcess);
					activityProcess.addEdge(edge);
				}
			}
		}
		
		return activityProcess;
	}
	
	private List<Node> findTargetActivities(Node node) {
		LinkedList<Node> targets = new LinkedList<Node>();
		this.checkTargets(node, targets);
		return targets;
	}

	private void checkTargets(Node node, LinkedList<Node> targets) {
		for (Node target : node.getProcess().getTargetNodes(node)) {
			//if (!ids.contains(node.getId())) {
				if (NodeTypes.isActivity(target)) {
					targets.add(target);
				}
				else {
					this.checkTargets(target, targets);
				}
			//}
		}
	}
}
