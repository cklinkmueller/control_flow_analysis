package bpm.matching.properties.graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import bpm.model.Node;
import bpm.model.NodeTypes;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class NeighborhoodUtil {
	public static Set<Node> determineNeighborhood(Node node) {
		HashSet<Node> neighbors = new HashSet<Node>();
		
		neighbors.addAll(determineUpstreamNeighborhood(node));
		neighbors.addAll(determineDownstreamNeighborhood(node));
		
		return neighbors;
	}

	public static Set<Node> determineDownstreamNeighborhood(Node node) {
		return determineNeighborhood(node, NeighborhoodCommand.getDownstreamNeighborhoodCommand());
	}

	public static  Set<Node> determineUpstreamNeighborhood(Node node) {
		return determineNeighborhood(node, NeighborhoodCommand.getUpstreamNeighborhoodCommand());
	}
	
	private static Set<Node> determineNeighborhood(Node node, NeighborhoodCommand neighborhoodCommand) {
		HashSet<Node> neighbors = new HashSet<Node>();
		
		LinkedList<Node> visited = new LinkedList<Node>();
		LinkedList<Node> visit = new LinkedList<Node>();
		visit.add(node);
		
		do {
			LinkedList<Node> newvisit = new LinkedList<Node>();
			for (Node v : visit) {
				for (Node neighbor : neighborhoodCommand.getNeighborNodes(v)) {
					if (NodeTypes.isActivity(neighbor)) {
						neighbors.add(neighbor);
					}
					else if (!visited.contains(neighbor)) {
						visited.add(neighbor);
						newvisit.add(neighbor);
					}
				}
			}
			visit = newvisit;
		} while (visit.size() != 0);
		
		return neighbors;
	}
}
