package bpm.matching.properties.graph;

import java.util.Collection;

import bpm.matching.properties.Property;
import bpm.model.Node;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class GraphNeighborhoodProperty extends Property {

	public GraphNeighborhoodProperty(Collection<Process> processes) {
		super(processes);
	}
	
	@Override
	protected double determineValue(Node node) {
		return NeighborhoodUtil.determineNeighborhood(node).size();
	}

}
