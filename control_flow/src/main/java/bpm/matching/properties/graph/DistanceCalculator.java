package bpm.matching.properties.graph;

import java.util.LinkedList;
import java.util.Map;

import bpm.model.Node;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class DistanceCalculator {
	private NeighborhoodCommand initializeCommand;
	private NeighborhoodCommand traverseCommand;
	private ActivityGraphTransformer transformer;
	
	public DistanceCalculator(NeighborhoodCommand initializeCommand, NeighborhoodCommand traverseCommand) {
		this.initializeCommand = initializeCommand;
		this.traverseCommand = traverseCommand;
		this.transformer = new ActivityGraphTransformer();
	}
	
	public double initialize(Process process, Map<Long, Double> nodeDistances) {
		Process activityGraph = transformer.transformActivityGraph(process);
			
		LinkedList<Node> nodes = new LinkedList<Node>();
		this.initialize(activityGraph, nodes, nodeDistances);
			
		double max = 0;
			
		while (!nodes.isEmpty()) {
			Node node = this.getMinimumNode(nodes, nodeDistances);
			
			for (Node source : this.traverseCommand.getNeighborNodes(node)) {
				this.updateDistance(source, node, nodeDistances);
			}
			
			double val = nodeDistances.get(node.getId());
			max = Math.max(max, val);
		}
		
		return max;
	}

	private void updateDistance(Node source, Node node, Map<Long, Double> nodeDistances) {
		double distance = nodeDistances.get(node.getId()) + 1;
		
		if (distance < nodeDistances.get(source.getId())) {
			nodeDistances.put(source.getId(), distance);
		}
	}

	private Node getMinimumNode(LinkedList<Node> nodes, Map<Long, Double> nodeDistances) {
		double min = Double.POSITIVE_INFINITY;
		Node sel = null;
		
		for (Node node : nodes) {
			double val = nodeDistances.get(node.getId());
			if (sel == null || val < min) {
				min = val;
				sel = node;
			}
		}
		
		nodes.remove(sel);
		return sel;
	}

	private void initialize(Process activityGraph, LinkedList<Node> nodes, Map<Long, Double> nodeDistances) {
		for (Node node : activityGraph.getNodes()) {
			nodes.add(node);
			
			if (this.initializeCommand.getNeighborNodes(node).size() == 0) {
				nodeDistances.put(node.getId(), 0d);
			}
			else {
				nodeDistances.put(node.getId(), Double.POSITIVE_INFINITY);
			}			
		}
	}
}
