package bpm.matching.properties.graph;

import java.util.Collection;

import bpm.model.Node;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public abstract class NeighborhoodCommand {
	public abstract Collection<Node> getNeighborNodes(Node node);
	
	public static NeighborhoodCommand getUpstreamNeighborhoodCommand() {
		return new UpstreamNeighborhoodCommand();
	}
	
	public static NeighborhoodCommand getDownstreamNeighborhoodCommand() {
		return new DownstreamNeighborhoodCommand();
	}
	
	private static class DownstreamNeighborhoodCommand extends NeighborhoodCommand {

		@Override
		public Collection<Node> getNeighborNodes(Node node) {
			return node.getProcess().getTargetNodes(node);
		}
		
	}
	
	private static class UpstreamNeighborhoodCommand extends NeighborhoodCommand {

		@Override
		public Collection<Node> getNeighborNodes(Node node) {
			return node.getProcess().getSourceNodes(node);
		}
		
	}
}
