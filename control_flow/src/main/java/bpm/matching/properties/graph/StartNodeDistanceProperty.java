package bpm.matching.properties.graph;

import java.util.Collection;

import bpm.matching.properties.Property;
import bpm.model.Node;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class StartNodeDistanceProperty extends Property {
	public StartNodeDistanceProperty(Collection<Process> processes) {
		super(processes);	
	}
	
	@Override
	protected void initialize(Collection<Process> processes) {
		DistanceCalculator calculator = new DistanceCalculator(NeighborhoodCommand.getUpstreamNeighborhoodCommand(), NeighborhoodCommand.getDownstreamNeighborhoodCommand());
		
		for (Process process : processes) {
			double val = calculator.initialize(process, this.values);
			this.maxima.put(process.getId(), val);
		}
	}
	
	@Override
	protected double determineValue(Node node) {
		return 0;
	}

}
