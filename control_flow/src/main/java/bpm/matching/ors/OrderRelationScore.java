package bpm.matching.ors;

import java.util.ArrayList;
import java.util.List;

import bpm.matching.properties.Property;
import bpm.model.Alignment;
import bpm.model.Node;
import bpm.model.ProcessUtilities;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class OrderRelationScore {
	
	public double getOrderRelationScore(Property property, List<Alignment> alignments) {
		double score = 0;
		
		for (Alignment alignment : alignments) {
			score += this.getOrderRelationScore(property, alignment);
		}
		
		return score / alignments.size();
	}
	
	public double getOrderRelationScore(Property property, Alignment alignment) {
		ArrayList<ValuePair> pairs = this.extractPairs(property, alignment);
		
		double score = 0;
		int num = 0;
		
		for (int a = 0; a < pairs.size() - 1; a++) {
			ValuePair vp1 = pairs.get(a);
			for (int b = a + 1; b < pairs.size(); b++) {
				ValuePair vp2 = pairs.get(b);
				double v1 = vp1.value1 - vp2.value1;
				double v2 = vp1.value2 - vp2.value2;
				
				if (v1 * v2 >= 0) {
					score += 1;
				}
				
				num++;
			}
		}
		
		return num == 0 ? 0 : score / num;
	}
	
	private ArrayList<ValuePair> extractPairs(Property property, Alignment alignment) {
		ArrayList<ValuePair> pairs = new ArrayList<ValuePair>();
		
		double max1 = property.getMaximum(alignment.getProcess1());
		double max2 = property.getMaximum(alignment.getProcess2());
		
		for (Node ac1 : ProcessUtilities.getActivities(alignment.getProcess1())) {
			for (Node ac2 : ProcessUtilities.getActivities(alignment.getProcess2())) {
				if (alignment.areCorresponding(ac1, ac2)) {
					ValuePair p = new ValuePair();
					p.value1 = max1 == 0 ? 0 : property.getAttributeValue(ac1) / max1;
					p.value2 = max2 == 0 ? 0 : property.getAttributeValue(ac2) / max2;
					pairs.add(p);
				}
			}
		}
		
		return pairs;
	}

	private class ValuePair {
		double value1;
		double value2;
	}
}
