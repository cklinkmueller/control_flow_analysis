package bpm.matching.fragments;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

import bpm.Configuration;
import bpm.io.ModelReader;
import bpm.matching.properties.hierarchy.RpstActivityNode;
import bpm.matching.properties.hierarchy.RpstCreator;
import bpm.model.Process;
import bpm.model.ProcessUtilities;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class FragmentCountProgram {
	public static void main(String[] args) {
		for (String dataset : Configuration.developmentDatasets) {
			System.out.println(dataset);
			
			ModelReader reader = Configuration.datasetModelReaders.get(dataset);
			List<Process> processes = reader.read(Configuration.datasetModelsFolder.get(dataset));
		
			int rpstCount = countRpstFrags(processes);
			System.out.println("\trpst: " + rpstCount);
			
			int maxSize = getMaxSize(processes, dataset);
			BigInteger subgraphCount = countConnectedSubgraphs(processes, maxSize);
			System.out.println("\tconnected: " + subgraphCount.toString());
			
			BigInteger arbitraryCount = countArbitrarySubgraphs(processes);
			System.out.println("\tarbitrary: " + arbitraryCount.toString());
		}
	}
	
	private static BigInteger countArbitrarySubgraphs(List<Process> processes) {
		HashMap<Integer, BigInteger> faculties = new HashMap<Integer, BigInteger>();
		HashMap<Integer, BigInteger> combinations = new HashMap<Integer, BigInteger>();
		
		BigInteger fac = BigInteger.valueOf(1);
		faculties.put(0, fac);
		
		for (int a = 1; a < 49; a++) {
			fac = fac.multiply(BigInteger.valueOf(a));
			faculties.put(a, fac);
		}
		
		for (int n = 1; n < 49; n++) {
			BigInteger sum = BigInteger.ZERO;
			BigInteger counter = faculties.get(n);
			for (int k = 1; k <= n; k++) {
				BigInteger kfac = faculties.get(k);
				BigInteger nkfac = faculties.get(n - k);
				BigInteger denominator = kfac.multiply(nkfac);
				sum = sum.add(counter.divide(denominator));
			}
			combinations.put(n, sum);
		}
		
		BigInteger sum = BigInteger.valueOf(0);
		
		for (Process process : processes) {
			int acs = ProcessUtilities.countActivities(process);
			sum = sum.add(combinations.get(acs));
		}
		
		return sum;
	}

	private static int getMaxSize(List<Process> processes, String dataset) {
		int max = 0;
		
		for (Process process : processes) {
			max = Math.max(max, ProcessUtilities.countActivities(process));
		}
		
		return max;
		
		// if limited to the maximum fragment size in the datasets
		// return dataset.equals("BR") ? 8 : 9
	}

	private static BigInteger countConnectedSubgraphs(List<Process> processes, int maxSize) {
		ArbitrarySubGraphCounter counter = new ArbitrarySubGraphCounter();
		return counter.countSubgraphs(processes);
	}

	private static int countRpstFrags(List<Process> processes) {
		RpstCreator creator = new RpstCreator();
		
		frags = 0;
		trivials = 0;
		
		for (Process p : processes) {
			RpstActivityNode root = creator.createRpst(p);
			countFrags(root);
		}
		
		return frags - trivials;
	}

	private static int frags = 0;
	private static int trivials = 0;
	
	private static void countFrags(RpstActivityNode root) {
		frags++;
		
		if (root.getType().equals(RpstActivityNode.TRIVIAL)) {
			trivials++;
		}
		else {
			for (RpstActivityNode dec : root.getDecendants()) {
				countFrags(dec);
			}
		}
	}
}
