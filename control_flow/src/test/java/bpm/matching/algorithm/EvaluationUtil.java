package bpm.matching.algorithm;

import java.util.LinkedList;
import java.util.List;

import bpm.model.Process;
import bpm.text.cooc.PorterCooccurrence;
import bpm.text.similarity.CooccurrenceComparer;
import bpm.text.similarity.LevenshteinComparer;
import bpm.text.similarity.LinWordNetComparer;
import bpm.text.similarity.PorterStemComparer;
import bpm.text.similarity.WordComparer;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class EvaluationUtil {
	public static final boolean[] PRUNING_OPTIONS = new boolean[]{false, true};
	private static WordComparer lin;
	private static WordComparer lev;
	private static WordComparer cco;

	public static List<WordComparer> getComparers(List<Process> processes) {
		List<WordComparer> comparers = new LinkedList<WordComparer>();
		
		lev = new PorterStemComparer(new LevenshteinComparer());
		lev.initialize(processes);
		comparers.add(lev);
		
		lin = new PorterStemComparer(new LinWordNetComparer());
		lin.initialize(processes);
		comparers.add(lin);
		
		cco = new CooccurrenceComparer(new PorterCooccurrence(processes, false), 2);
		cco.initialize(processes);
		comparers.add(cco);
		
		return comparers;
	}

	public static WordComparer getLin() {
		return lin;
	}

	public static WordComparer getLev() {
		return lev;
	}

	public static WordComparer getCco() {
		return cco;
	}
}
