package bpm.matching.algorithm;

import java.util.List;

import bpm.Configuration;
import bpm.io.AlignmentReader;
import bpm.io.ModelReader;
import bpm.matching.properties.graph.StartNodeDistanceProperty;
import bpm.model.Alignment;
import bpm.model.Process;
import bpm.text.similarity.WordComparer;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class EvaluationOPBOT {
	public static void main(String[] args) {
		for (String dataset : Configuration.datasets) {
			System.out.println(dataset);
			
			ModelReader reader = Configuration.datasetModelReaders.get(dataset);
			List<Process> processes = reader.read(Configuration.datasetModelsFolder.get(dataset));
		
			AlignmentReader alignmentReader = Configuration.datasetAlignmentReader.get(dataset);
			List<Alignment> alignments = alignmentReader.read(Configuration.datasetAlignmentsFolder.get(dataset), processes);
						
			ModelPairUtility util = new ModelPairUtility(new StartNodeDistanceProperty(processes));
			List<ModelPair> pairs = util.createModelPairs(alignments);
			
			EvaluationUtil.getComparers(processes);
			WordComparer lev = EvaluationUtil.getLev();
			WordComparer lin = EvaluationUtil.getLin();
			WordComparer cco = EvaluationUtil.getCco();
			
			OPBOT opbot = new OPBOT(lin, lev, cco);
			
			double start = System.currentTimeMillis();
			pairs = opbot.matchModelPairs(pairs);
			double end = System.currentTimeMillis();
				
			EffectivenessMeasures max = new EffectivenessMeasures(pairs);
			
			System.out.println("\ttime: " + ((end - start) / 1000) + "s");
			System.out.println("\tP" + (dataset.equals("UA") ? "mac" : "mic") + ": " + (dataset.equals("UA") ? max.getPmac() : max.getPmic()));
			System.out.println("\tR" + (dataset.equals("UA") ? "mac" : "mic") + ": " + (dataset.equals("UA") ? max.getRmac() : max.getRmic()));
			System.out.println("\tF" + (dataset.equals("UA") ? "mac" : "mic") + ": " + (dataset.equals("UA") ? max.getFmac() : max.getFmic()));
		}
	}
}
