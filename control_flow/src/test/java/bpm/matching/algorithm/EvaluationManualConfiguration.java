package bpm.matching.algorithm;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import bpm.Configuration;
import bpm.io.AlignmentReader;
import bpm.io.ModelReader;
import bpm.matching.properties.graph.StartNodeDistanceProperty;
import bpm.model.Alignment;
import bpm.model.Process;
import bpm.text.similarity.WordComparer;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class EvaluationManualConfiguration {
	private static final int[] SIZES = {1,2,3,6};
	private static List<WordComparer> COMPARERS;
	private static final DecimalFormat FORMAT = new DecimalFormat("0.000");
	
	public static void main(String[] args) {
		for (String dataset : Configuration.datasets) {
			System.out.println(dataset);
			
			ModelReader reader = Configuration.datasetModelReaders.get(dataset);
			List<Process> processes = reader.read(Configuration.datasetModelsFolder.get(dataset));
		
			AlignmentReader alignmentReader = Configuration.datasetAlignmentReader.get(dataset);
			List<Alignment> alignments = alignmentReader.read(Configuration.datasetAlignmentsFolder.get(dataset), processes);
			
			HashMap<Integer, List<Measures>> evaluations = new HashMap<Integer, List<Measures>>();
			COMPARERS = EvaluationUtil.getComparers(processes);
			
			ModelPairUtility util = new ModelPairUtility(new StartNodeDistanceProperty(processes));
			List<ModelPair> pairs = util.createModelPairs(alignments);
			
			evaluate(dataset, evaluations, pairs);
			
			print(evaluations);
		}
	}

	private static void print(HashMap<Integer, List<Measures>> evaluations) {
		for (Entry<Integer, List<Measures>> entry : evaluations.entrySet()) {
			double p = 0,
				   r = 0,
				   f = 0,
				   corrs = 0,
				   pairs = 0;
			
			for (Measures eval : entry.getValue()) {
				f += eval.f;
				p += eval.p;
				r += eval.r;
				corrs += eval.corr;
				pairs += eval.total;
			}
			
			f /= entry.getValue().size();
			r /= entry.getValue().size();
			p /= entry.getValue().size();
			corrs /= entry.getValue().size();
			pairs /= entry.getValue().size();
			
			System.out.print(entry.getKey());
			System.out.print("\t\t");
			System.out.print(FORMAT.format(p));
			System.out.print("\t\t");
			System.out.print(FORMAT.format(r));
			System.out.print("\t\t");
			System.out.print(FORMAT.format(f));
			System.out.print("\t\t");
			System.out.print(corrs);
			System.out.print("\t\t");
			System.out.print(pairs);
			System.out.print("\n");
		}
		System.out.println("\n");
	}

	private static void evaluate(String dataset, HashMap<Integer, List<Measures>> evaluations, List<ModelPair> pairs) {
		for (int groupSize : SIZES) {
			for (int a = 0; a < groupSize; a++) {
				List<Group> groups = extractGroups(groupSize, pairs);
				
				for (Group g : groups) {
					LinkedList<ModelPair> set1 = new LinkedList<ModelPair>();
					set1.addAll(g.pairs);
					
					LinkedList<ModelPair> set2 = new LinkedList<ModelPair>();
					for (ModelPair pair : pairs) {
						if (!set1.contains(pair)) {
							set2.add(pair);
						}
					}
					
					measure(set1, set2, evaluations, dataset.equals("UA") ? false : true);
				}
			}
		}
	}

	private static void measure(LinkedList<ModelPair> training, LinkedList<ModelPair> eval, HashMap<Integer, List<Measures>> evaluations, boolean mic) {
		Measures measures = optimize(training, mic);
		
		measures.bot.matchModelPairs(eval);
		EvaluationBOT.setCorresponding(eval, measures.threshold);
		EffectivenessMeasures effec = new EffectivenessMeasures(eval);
		
		measures.f = mic ? effec.getFmic() : effec.getFmac();
		measures.p = mic ? effec.getPmic() : effec.getPmac();
		measures.r = mic ? effec.getRmic() : effec.getRmac();	
		
		List<Measures> list = evaluations.get(training.size());
		if (list == null) {
			list = new LinkedList<Measures>();
			evaluations.put(training.size(), list);
		}
		list.add(measures);
	}
	
	private static Measures optimize(List<ModelPair> pairs, boolean mic) {
		Measures max = new Measures();
		
		for (WordComparer comparer : COMPARERS) {			
			for (boolean prune : EvaluationUtil.PRUNING_OPTIONS) {
				BagOfWordsSimilarity sim = new BagOfWordsSimilarity(comparer, prune);
				BOT bot = new BOT(sim);
				
				bot.matchModelPairs(pairs);
				
				for (double threshold : EvaluationBOT.getSimilarityValues(pairs)) {
					int cor = 0;
					int total = 0;
					for (ModelPair mp : pairs) {
						for (ActivityPair ap : mp.getActivityPairs()) {
							if (ap.getSimilarity() >= threshold) {
								ap.setCorresponding(true);
								cor++;
							}
							else {
								ap.setCorresponding(false);
							}
						}
						total += mp.getActivityPairs().size();
					}
					
					
					EffectivenessMeasures effec = new EffectivenessMeasures(pairs);

					if (max.f <= (mic ? effec.getFmic() : effec.getFmac())) {
						max.bot = bot;
						max.threshold = threshold;
						max.f = mic ? effec.getFmic() : effec.getFmac();
						max.p = mic ? effec.getPmic() : effec.getPmac();
						max.r = mic ? effec.getRmic() : effec.getRmac();
						max.corr = cor;
						max.total = total;
					}
				}
			}
		}
		
		return max;
	}

	private static List<Group> extractGroups(int groupSize, List<ModelPair> modelPairs) {
		LinkedList<Group> groups = new LinkedList<Group>();
		Random random = new Random();
		LinkedList<ModelPair> pairs = new LinkedList<ModelPair>(modelPairs);
		
		while (!pairs.isEmpty()) {
			Group g = new Group();
			
			while (g.pairs.size() < groupSize) {
				g.pairs.add(pairs.remove(random.nextInt(pairs.size())));
			}
			
			groups.add(g);
		}
		
		return groups;
	}

	private static class Group {
		public LinkedList<ModelPair> pairs = new LinkedList<ModelPair>();
	}
	
	private static class Measures {
		public BOT bot;
		public double threshold;
		
		public double f,
					  p,
					  r;
		private int corr,
					total;
	}
}
