package bpm.matching.algorithm;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import bpm.Configuration;
import bpm.io.AlignmentReader;
import bpm.io.ModelReader;
import bpm.matching.properties.graph.StartNodeDistanceProperty;
import bpm.model.Alignment;
import bpm.model.Process;
import bpm.text.similarity.CooccurrenceComparer;
import bpm.text.similarity.WordComparer;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class EvaluationBOT {
		
	public static void main(String[] args) {
		for (String dataset : Configuration.datasets) {
			System.out.println(dataset);
			
			ModelReader reader = Configuration.datasetModelReaders.get(dataset);
			List<Process> processes = reader.read(Configuration.datasetModelsFolder.get(dataset));
		
			AlignmentReader alignmentReader = Configuration.datasetAlignmentReader.get(dataset);
			List<Alignment> alignments = alignmentReader.read(Configuration.datasetAlignmentsFolder.get(dataset), processes);
						
			ModelPairUtility util = new ModelPairUtility(new StartNodeDistanceProperty(processes));
			List<ModelPair> pairs = util.createModelPairs(alignments);
			
			List<WordComparer> comparers = EvaluationUtil.getComparers(processes);
			
			EffectivenessMeasures max = null;
			
			for (WordComparer comp : comparers) {
				double minThresh = comp.getClass().equals(CooccurrenceComparer.class) ? 0.7 : 0.6;
				for (boolean prune : EvaluationUtil.PRUNING_OPTIONS) {
					BagOfWordsSimilarity sim = new BagOfWordsSimilarity(comp, prune);
					BOT bot = new BOT(sim);
					bot.matchModelPairs(pairs);
					
					List<Double> similarities = getSimilarityValues(pairs);
					for (double threshold : similarities) {
						if (threshold >= minThresh) {
							setCorresponding(pairs, threshold);
							EffectivenessMeasures m = new EffectivenessMeasures(pairs);
							
							if (max == null || (dataset.equals("UA") ? max.getFmac() < m.getFmac() : max.getFmic() < m.getFmic())) {
								max = m;
							}
						}
					}
				}
			}
			
			System.out.println("\tP" + (dataset.equals("UA") ? "mac" : "mic") + ": " + (dataset.equals("UA") ? max.getPmac() : max.getPmic()));
			System.out.println("\tR" + (dataset.equals("UA") ? "mac" : "mic") + ": " + (dataset.equals("UA") ? max.getRmac() : max.getRmic()));
			System.out.println("\tF" + (dataset.equals("UA") ? "mac" : "mic") + ": " + (dataset.equals("UA") ? max.getFmac() : max.getFmic()));
		}
	}

	public static void setCorresponding(List<ModelPair> pairs, double threshold) {
		for (ModelPair mp : pairs) {
			for (ActivityPair ap : mp.getActivityPairs()) {
				ap.setCorresponding(ap.getSimilarity() >= threshold);
			}
		}
	}

	public static List<Double> getSimilarityValues(List<ModelPair> pairs) {
		LinkedList<Double> sims = new LinkedList<Double>();
		
		for (ModelPair mp : pairs) {
			for (ActivityPair ap : mp.getActivityPairs()) {
				if (!sims.contains(ap.getSimilarity())) {
					sims.add(ap.getSimilarity());
				}
			}
		}
		
		Collections.sort(sims, (d1, d2) -> Double.compare(d2, d1));
		
		return sims;
	}
}
