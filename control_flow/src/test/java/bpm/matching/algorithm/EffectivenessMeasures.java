package bpm.matching.algorithm;

import java.util.List;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class EffectivenessMeasures {
	private double fmic;
	private double fmac;
	private double pmic;
	private double pmac;
	private double rmic;
	private double rmac;
	
	public EffectivenessMeasures(List<ModelPair> pairs) {
		double[] fs = new double[pairs.size()];
		double[] ps = new double[pairs.size()];
		double[] rs = new double[pairs.size()];
		
		double tps = 0;
		double fps = 0;
		double fns = 0;
		
		for (int a = 0; a < pairs.size(); a++) {
			ModelPair mp = pairs.get(a);
			
			double tp = 0;
			double fp = 0;
			double fn = 0;
			
			for (ActivityPair ap : mp.getActivityPairs()) {
				if (ap.isGoldCorresponding()) {
					if (ap.isCorresponding()) {
						tp++;
					}
					else {
						fn++;
					}
				}
				else {
					if (ap.isCorresponding()) {
						fp++;
					}
				}
			}
		
			ps[a] = this.getPrecision(tp, fp);
			rs[a] = this.getRecall(tp, fn);
			fs[a] = this.getFmeasure(ps[a], rs[a]);
			
			tps += tp;
			fps += fp;
			fns += fn;
		}
		
		this.pmic = this.getPrecision(tps, fps);
		this.rmic = this.getRecall(tps, fns);
		this.fmic = this.getFmeasure(this.pmic, this.rmic);
		
		this.pmac = this.getAverage(ps);
		this.rmac = this.getAverage(rs);
		this.fmac = this.getAverage(fs);
	}
	
	private double getAverage(double[] array) {
		double sum = 0;
		
		for (double val : array) {
			sum += val;
		}
		
		return sum / array.length;
	}

	private double getFmeasure(double p, double r) {
		return r + p == 0 ? 0 : 2 * r * p / (r + p);
	}

	private double getPrecision(double tp, double fp) {
		return (tp + fp) == 0 ? 1 : tp / (tp + fp);
	}

	private double getRecall(double tp, double fn) {
		return (tp + fn) == 0 ? 1 : tp / (tp + fn);
	}

	public double getFmic() {
		return fmic;
	}

	public void setFmic(double fmic) {
		this.fmic = fmic;
	}

	public double getFmac() {
		return fmac;
	}

	public void setFmac(double fmac) {
		this.fmac = fmac;
	}

	public double getPmic() {
		return pmic;
	}

	public void setPmic(double pmic) {
		this.pmic = pmic;
	}

	public double getPmac() {
		return pmac;
	}

	public void setPmac(double pmac) {
		this.pmac = pmac;
	}

	public double getRmic() {
		return rmic;
	}

	public void setRmic(double rmic) {
		this.rmic = rmic;
	}

	public double getRmac() {
		return rmac;
	}

	public void setRmac(double rmac) {
		this.rmac = rmac;
	}
}
