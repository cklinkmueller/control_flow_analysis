package bpm.matching.ors;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import bpm.io.ModelReader;
import bpm.io.PetriNetModelReader;
import bpm.matching.properties.graph.StartNodeDistanceProperty;
import bpm.model.Alignment;
import bpm.model.Node;
import bpm.model.Process;
import bpm.model.ProcessUtilities;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class BRContestMatcherSelection {
	private static final String PATH = "./src/test/resources/contest";
	private static final String MODELS = "./src/test/resources/contestmodels/";
	private static final String DATASET = "dataset2";
	
	public static void main(String[] args) {
		ModelReader reader = new PetriNetModelReader();
		List<Process> processes = reader.read(MODELS + DATASET);
		
		OrderRelationScore ors = new OrderRelationScore();
		StartNodeDistanceProperty prop = new StartNodeDistanceProperty(processes);
		
		HashMap<String, List<Alignment>> alignments = getAlignments(processes);
		
		for (Entry<String, List<Alignment>> matcherEntry : alignments.entrySet()) {
			System.out.print(matcherEntry.getKey());
			System.out.print(": ");
			
			double score = ors.getOrderRelationScore(prop, matcherEntry.getValue());
			System.out.println(score);
		}
	}

	private static HashMap<String, List<Alignment>> getAlignments(List<Process> processes) {
		HashMap<String, List<Alignment>> alignments = new HashMap<String, List<Alignment>>();
		
		File file = new File(PATH);
		for (File matcherFolder : file.listFiles()) {
			String matcher = matcherFolder.getName();
			
			List<Alignment> matcherAlignments = new LinkedList<Alignment>();
			for (File datasetFolder : matcherFolder.listFiles()) {
				if (datasetFolder.getName().equals(DATASET)) {
					for (File alignmentFile : datasetFolder.listFiles()) {
						Alignment alignment = readAlignment(processes, alignmentFile);
						if (alignment != null) {
							matcherAlignments.add(alignment);
						}
					}
				}
			}
			
			alignments.put(matcher, matcherAlignments);
		}
		
		return alignments;
	}

	private static Alignment readAlignment(List<Process> processes, File file) {
		String[] names = file.getName().replace(".rdf","").split("-");
		Process p1 = getProcess(processes, names[0]);
		Process p2 = getProcess(processes, names[1]);
		
		if (p1 == null || p2 == null) {
			return null;
		}
		
		Alignment alignment = new Alignment(p1, p2);
		
		try {
			XPath xPath = XPathFactory.newInstance().newXPath();
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			doc.getDocumentElement().normalize();
			
			NodeList nodes = doc.getElementsByTagName("Cell");
			for (int a = 0; a < nodes.getLength(); a++) {
				Element cell = (Element)nodes.item(a);
				Element e1 = (Element)xPath.evaluate("./entity1",cell, XPathConstants.NODE);
				Element e2 = (Element)xPath.evaluate("./entity2",cell, XPathConstants.NODE);
				
				String[] at = e1.getAttribute("rdf:resource").replace("http://", "").split("#");
				String id1 = at[at.length - 1];
				
				at = e2.getAttribute("rdf:resource").replace("http://", "").split("#");
				String id2 = at[at.length - 1];
				
				Node ac1 = getNode(p1, id1);
				Node ac2 = getNode(p2, id2);
				
				if (ac1 != null && ac2 != null) {
					alignment.setCorresponding(ac1, ac2, true);
				}
			}			
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return alignment;
	}

	private static Node getNode(Process process, String id) {
		for (Node n : ProcessUtilities.getActivities(process)) {
			if (n.getOldId().equals(id)) {
				return n;
			}
		}
		return null;
	}

	private static Process getProcess(List<Process> processes, String name) {
		for (Process process : processes) {
			if (process.getName().equals(name)) {
				return process;
			}
		}
		return null;
	}
}
