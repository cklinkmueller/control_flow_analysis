package bpm.matching.ors;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import bpm.Configuration;
import bpm.io.AlignmentReader;
import bpm.io.ModelReader;
import bpm.matching.properties.Property;
import bpm.matching.properties.exsem.StrictOrderProperty;
import bpm.matching.properties.graph.EndNodeDistanceProperty;
import bpm.matching.properties.graph.StartNodeDistanceProperty;
import bpm.matching.properties.hierarchy.RpstDepthProperty;
import bpm.model.Alignment;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class GoldStandardOrsTest {
	public static void main(String[] args) throws IOException {
		OrderRelationScore ors = new OrderRelationScore();
		
		for (String dataset : Configuration.developmentDatasets) {
			System.out.println(dataset);

			ModelReader reader = Configuration.datasetModelReaders.get(dataset);
			List<Process> processes = reader.read(Configuration.datasetModelsFolder.get(dataset));
		
			AlignmentReader alignmentReader = Configuration.datasetAlignmentReader.get(dataset);
			List<Alignment> alignments = alignmentReader.read(Configuration.datasetAlignmentsFolder.get(dataset), processes);
			
			List<Property> properties = getProperties(processes, dataset);
			
			for (Property prop : properties) {
				double score = ors.getOrderRelationScore(prop, alignments);
				System.out.println("\t" + prop.getClass().getSimpleName() + ": " + score);
			}
			
			System.out.println();
		}
	}

	public static List<Property> getProperties(List<Process> processes, String dataset) {
		LinkedList<Property> properties = new LinkedList<Property>();
		
		properties.add(new StartNodeDistanceProperty(processes));
		properties.add(new EndNodeDistanceProperty(processes));
		
		properties.add(new RpstDepthProperty(processes));
		
		if (dataset.equals("BR")) {
			properties.add(new StrictOrderProperty(processes));
		}
		
		return properties;
	}
}
