package bpm.matching.ors;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import bpm.io.EpcModelReader;
import bpm.io.ModelReader;
import bpm.matching.properties.graph.StartNodeDistanceProperty;
import bpm.model.Alignment;
import bpm.model.Node;
import bpm.model.Process;
import bpm.model.ProcessUtilities;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class AMContestMatcherSelection {
	private static final String PATH = "./src/test/resources/contest";
	private static final String MODELS = "./src/test/resources/contestmodels/";
	private static final String DATASET = "dataset3";
	private static final String[] IGNORE = {"testcase31", "testcase32", "testcase33", "testcase34","testcase35", "testcase36"};
	
	public static void main(String[] args) {
		List<Process> processes = readModels();
		
		OrderRelationScore ors = new OrderRelationScore();
		StartNodeDistanceProperty prop = new StartNodeDistanceProperty(processes);
		
		HashMap<String, List<Alignment>> alignments = getAlignments(processes);
		
		for (Entry<String, List<Alignment>> matcherEntry : alignments.entrySet()) {
			System.out.print(matcherEntry.getKey());
			System.out.print(": ");
			
			double score = ors.getOrderRelationScore(prop, matcherEntry.getValue());
			// if a matcher suggested no correspondences for a model pair,
			// there is no alignment in the list, 
			// As the score of empty alignments is 0,
			// we normalize the score here
			score = score * matcherEntry.getValue().size() / 30;
			System.out.println(score);
		}
	}

	private static List<Process> readModels() {
		ModelReader reader = new EpcModelReader();
		LinkedList<Process> processes = new LinkedList<Process>();
		
		File modelFolder = new File(MODELS + DATASET);
		for (File caseFolder : modelFolder.listFiles()) {
			String cas = caseFolder.getName();
			
			File modelFile = new File(caseFolder, "source-model.epml");
			Process process = reader.read(modelFile.getAbsolutePath()).get(0);
			process.setName("http://sap/" + cas + "/source/");
			processes.add(process);
			
			modelFile = new File(caseFolder, "target-model.epml");
			process = reader.read(modelFile.getAbsolutePath()).get(0);
			process.setName("http://sap/" + cas + "/target/");
			processes.add(process);
		}
		
		return processes;
	}

	private static HashMap<String, List<Alignment>> getAlignments(List<Process> processes) {
		HashMap<String, List<Alignment>> alignments = new HashMap<String, List<Alignment>>();
		
		File file = new File(PATH);
		for (File matcherFolder : file.listFiles()) {
			String matcher = matcherFolder.getName();
			
			List<Alignment> matcherAlignments = new LinkedList<Alignment>();
			for (File datasetFolder : matcherFolder.listFiles()) {
				if (datasetFolder.getName().equals(DATASET)) {
					for (File alignmentFile : datasetFolder.listFiles()) {
						if (!ignore(alignmentFile.getName())) {
							readAlignment(matcherAlignments, processes, alignmentFile);	
						}
					}
				}
			}
			
			alignments.put(matcher, matcherAlignments);
		}
		
		return alignments;
	}
	
	private static boolean ignore(String name) {
		for (String ig : IGNORE) {
			if (name.contains(ig)) {
				return true;
			}
		}
		
		return false;
	}

	private static void readAlignment(List<Alignment> alignments, List<Process> processes, File file) {
		try {
			XPath xPath = XPathFactory.newInstance().newXPath();
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			doc.getDocumentElement().normalize();
			
			NodeList nodes = doc.getElementsByTagName("Cell");
			for (int a = 0; a < nodes.getLength(); a++) {
				Element cell = (Element)nodes.item(a);
				Element e1 = (Element)xPath.evaluate("./entity1",cell, XPathConstants.NODE);
				Element e2 = (Element)xPath.evaluate("./entity2",cell, XPathConstants.NODE);
				
				String name = e1.getAttribute("rdf:resource");
				int index = name.lastIndexOf("/") + 1;
				String pid1 = name.substring(0, index);
				String id1 = name.substring(index);
				Process p1 = getProcess(processes, pid1);
				
				name = e2.getAttribute("rdf:resource");
				index = name.lastIndexOf("/") + 1;
				String pid2 = name.substring(0, index);
				String id2 = name.substring(index);
				Process p2 = getProcess(processes, pid2);
				
				Alignment alignment = getAlignment(alignments, p1, p2);
				
				Node ac1 = getNode(p1, id1);
				Node ac2 = getNode(p2, id2);
				
				if (ac1 != null && ac2 != null) {
					alignment.setCorresponding(ac1, ac2, true);
				}
			}			
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static Alignment getAlignment(List<Alignment> alignments, Process p1, Process p2) {
		for (Alignment al : alignments) {
			if (al.getProcess1() == p1 && al.getProcess2() == p2) {
				return al;
			}
		}
		
		Alignment alignment = new Alignment(p1, p2);
		alignments.add(alignment);
		return alignment;
	}

	private static Node getNode(Process process, String id) {
		for (Node n : ProcessUtilities.getActivities(process)) {
			if (n.getOldId().equals(id)) {
				return n;
			}
		}
				
		return null;
	}

	private static Process getProcess(List<Process> processes, String name) {
		for (Process process : processes) {
			if (process.getName().equals(name)) {
				return process;
			}
		}
		
		return null;
	}
}
