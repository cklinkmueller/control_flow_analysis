package bpm.matching.ors;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import bpm.Configuration;
import bpm.io.AlignmentReader;
import bpm.io.ModelReader;
import bpm.matching.properties.Property;
import bpm.model.Alignment;
import bpm.model.Node;
import bpm.model.Process;
import bpm.model.ProcessUtilities;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class RandomAlignmentTest {
	private final static double[] THRESHOLDS = {0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1};
	
	public static void main(String[] args) {
		for (String dataset : Configuration.developmentDatasets) {
			System.out.println(dataset);
			
			ModelReader reader = Configuration.datasetModelReaders.get(dataset);
			List<Process> processes = reader.read(Configuration.datasetModelsFolder.get(dataset));
		
			AlignmentReader alignmentReader = Configuration.datasetAlignmentReader.get(dataset);
			List<Alignment> alignments = alignmentReader.read(Configuration.datasetAlignmentsFolder.get(dataset), processes);
		
			List<Property> properties = GoldStandardOrsTest.getProperties(processes, dataset);
			
			List<ActivityPair> correspondences = new LinkedList<ActivityPair>();
			List<ActivityPair> noncorrespondences = new LinkedList<ActivityPair>();
			clearAlignments(alignments, correspondences, noncorrespondences);
			
			StringBuilder builder = new StringBuilder();
			builder.append("fmic,orssta,orsend,orsdep");
			if (properties.size() == 4) {
				builder.append(",orsstrict");
			}
			builder.append("\n");
			
			generateAlignments(builder, alignments, correspondences, noncorrespondences, properties);
		
			String folder = Configuration.datasetOutputFolder.get(dataset);
			BufferedWriter writer = null;
			
			try {
				writer = new BufferedWriter(new FileWriter(new File(folder, "ors_random_alignments.csv")));
				writer.write(builder.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
			finally {
				if (writer != null) {
					try {
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	private static void generateAlignments(StringBuilder builder, List<Alignment> alignments, List<ActivityPair> correspondences, List<ActivityPair> noncorrespondences, List<Property> properties) {
		int[] values = new int[20];
		Random random = new Random();
		OrderRelationScore ors = new OrderRelationScore();
		
		for (int a = 0; a < 1000; a++) {
			// determine f-measure
			double fmic = 0;
			int pos = 0;
			int neg = 0;
			while (true) {
				pos = random.nextInt(correspondences.size());
				neg = random.nextInt(noncorrespondences.size());
				
				double p = ((double)pos) / (pos + neg);
				double r = ((double)pos) / correspondences.size();
				double f = (p+r == 0) ? 0 : 2 * p * r / (p + r);
				
				if (f <= 1 && f > 0) {
					int b = 0;
					while (!(THRESHOLDS[b] <= f && f < THRESHOLDS[b+1])) {
						b++;
					}
					if (values[b] < 50) {
						values[b]++;
						fmic = f;
						break;
					}
				}
			}
			
			// randomly select true positives & negatives
			List<ActivityPair> truepositives = draw(random, correspondences, pos);
			List<ActivityPair> falsepositives = draw(random, noncorrespondences, neg);
			
			// determine order relation scores
			builder.append(Double.toString(fmic));
			for (Property prop : properties) {
				builder.append(",");
				builder.append(Double.toString(ors.getOrderRelationScore(prop, alignments)));
			}
			builder.append("\n");
			
			// put suggestions back into containers
			shuffleBack(truepositives, correspondences);
			shuffleBack(falsepositives, noncorrespondences);
		}
	}

	private static void shuffleBack(List<ActivityPair> from, List<ActivityPair> to) {
		while (from.size() > 0) {
			ActivityPair ap = from.remove(0);
			ap.alignment.setCorresponding(ap.activity1, ap.activity2, false);
			to.add(ap);
		}
	}

	private static List<ActivityPair> draw(Random random, List<ActivityPair> pairs, int size) {
		List<ActivityPair> sample = new LinkedList<ActivityPair>();
		
		while (sample.size() < size) {
			ActivityPair ap = pairs.remove(random.nextInt(pairs.size()));
			ap.alignment.setCorresponding(ap.activity1, ap.activity2, true);
			sample.add(ap);
		}
		
		return sample;
	}

	private static void clearAlignments(List<Alignment> alignments, List<ActivityPair> correspondences, List<ActivityPair> noncorrespondences) {
		for (Alignment al : alignments) {
			for (Node ac1 : ProcessUtilities.getActivities(al.getProcess1())) {
				for (Node ac2 : ProcessUtilities.getActivities(al.getProcess2())) {
					if (al.areCorresponding(ac1, ac2)) {
						correspondences.add(new ActivityPair(al, ac1, ac2));
						al.setCorresponding(ac1, ac2, false);
					}
					else {
						noncorrespondences.add(new ActivityPair(al, ac1, ac2));
					}
				}
			}
		}
	}

	private static class ActivityPair {
		public Alignment alignment;
		public Node activity1;
		public Node activity2;
		
		public ActivityPair(Alignment al, Node ac1, Node ac2) {
			this.alignment = al;
			this.activity1 = ac1;
			this.activity2 = ac2;
		}
	}
	
}
