package bpm.matching.properties;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import bpm.Configuration;
import bpm.io.AlignmentReader;
import bpm.io.ModelReader;
import bpm.matching.properties.exsem.ExclusiveProperty;
import bpm.matching.properties.exsem.InterleavingProperty;
import bpm.matching.properties.exsem.StrictOrderProperty;
import bpm.matching.properties.graph.EndNodeDistanceProperty;
import bpm.matching.properties.graph.GraphNeighborhoodProperty;
import bpm.matching.properties.graph.StartNodeDistanceProperty;
import bpm.matching.properties.hierarchy.RpstDepthProperty;
import bpm.matching.properties.hierarchy.RpstNeighborhoodProperty;
import bpm.model.Alignment;
import bpm.model.Node;
import bpm.model.Process;
import bpm.model.ProcessUtilities;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class PropertyExport {
	public static void main(String[] args) throws IOException {
		for (String dataset : Configuration.developmentDatasets) {
			System.out.println(dataset);

			ModelReader reader = Configuration.datasetModelReaders.get(dataset);
			List<Process> processes = reader.read(Configuration.datasetModelsFolder.get(dataset));
		
			AlignmentReader alignmentReader = Configuration.datasetAlignmentReader.get(dataset);
			List<Alignment> alignments = alignmentReader.read(Configuration.datasetAlignmentsFolder.get(dataset), processes);
			
			List<Property> properties = getProperties(processes, dataset);
			
			StringBuilder builder = new StringBuilder();
			builder.append("id,corr,sta,end,nei,dep,sib");
			
			if (properties.size() == 8) {
				builder.append(",strict,inter,excl");
			}
			
			builder.append("\n");
			
			int id = 0;
			for (Alignment al : alignments) {
				for (Node ac1 : ProcessUtilities.getActivities(al.getProcess1())) {
					for (Node ac2 : ProcessUtilities.getActivities(al.getProcess2())) {
						builder.append(Integer.toString(id++));
						builder.append(",");
						builder.append(Boolean.toString(al.areCorresponding(ac1, ac2)));
						
						for (Property prop : properties) { 
							builder.append(",");
							builder.append(Double.toString(prop.getIndicator(ac1, ac2)));
						}
						
						builder.append("\n");
					}
				}
			}	
			
			String folder = Configuration.datasetOutputFolder.get(dataset);
			BufferedWriter writer = null;
			
			try {
				writer = new BufferedWriter(new FileWriter(new File(folder, "properties.csv")));
				writer.write(builder.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			finally {
				if (writer != null) {
					writer.close();
				}
			}
			
		}
	}

	public static List<Property> getProperties(List<Process> processes, String dataset) {
		LinkedList<Property> properties = new LinkedList<Property>();
		
		properties.add(new StartNodeDistanceProperty(processes));
		properties.add(new EndNodeDistanceProperty(processes));
		properties.add(new GraphNeighborhoodProperty(processes));
		
		properties.add(new RpstDepthProperty(processes));
		properties.add(new RpstNeighborhoodProperty(processes));

		if (dataset.equals("BR")) {
			properties.add(new StrictOrderProperty(processes));
			properties.add(new InterleavingProperty(processes));
			properties.add(new ExclusiveProperty(processes));
		}
		
		return properties;
	}
}
