package bpm.matching.properties;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import bpm.Configuration;
import bpm.io.AlignmentReader;
import bpm.io.ModelReader;
import bpm.model.Alignment;
import bpm.model.Node;
import bpm.model.Process;
import bpm.model.ProcessUtilities;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class InfoGainProgram {
	
	public static void main(String[] args) {
		for (String dataset : Configuration.developmentDatasets) {
			System.out.println(dataset);

			ModelReader reader = Configuration.datasetModelReaders.get(dataset);
			List<Process> processes = reader.read(Configuration.datasetModelsFolder.get(dataset));
		
			AlignmentReader alignmentReader = Configuration.datasetAlignmentReader.get(dataset);
			List<Alignment> alignments = alignmentReader.read(Configuration.datasetAlignmentsFolder.get(dataset), processes);
			
			List<Property> properties = PropertyExport.getProperties(processes, dataset);
						
			for (Property prop : properties) { 
				System.out.print("\t" + prop.getClass().getSimpleName() + ": ");	
				
				LinkedList<Pair> pairs = getPairs(prop, alignments);
				double gain = determineInformationGain(pairs);
				System.out.println(gain);
			}
		}
	}



	private static LinkedList<Pair> getPairs(Property prop, List<Alignment> alignments) {
		LinkedList<Pair> pairs = new LinkedList<Pair>();
		
		for (Alignment al : alignments) {
			for (Node ac1 : ProcessUtilities.getActivities(al.getProcess1())) {
				for (Node ac2 : ProcessUtilities.getActivities(al.getProcess2())) {
					Pair pair = new Pair();
					pair.corresponding = al.areCorresponding(ac1, ac2);
					pair.value = prop.getIndicator(ac1, ac2);
					pairs.add(pair);
				}
			}
		}
		
		return pairs;
	}

	private static double determineInformationGain(LinkedList<Pair> pairs) {
		double entropy = determineEntropy(pairs);
	
		Collections.sort(pairs, new Comparator<Pair>() {
			public int compare(Pair o1, Pair o2) {
				return Double.compare(o1.value, o2.value);
			}
		});
		
		double size = pairs.size();
		double maxgain = 0;
		
		for (int a = 1; a < pairs.size() - 1; a++) {
			
			double e1 = determineEntropy(pairs.subList(0, a));
			double e2 = determineEntropy(pairs.subList(a, pairs.size()));
			double gain = entropy - ((a / size) * e1 + ((size - a) / size) * e2);
			
			if (maxgain < gain) {
				maxgain = gain;
			}
		}		
		
		return maxgain;
	}

	private static double determineEntropy(Collection<Pair> pairs) {
		int num = 0;
		for (Pair pair : pairs) {
			if (pair.corresponding) {
				num++;
			}
		}
		
		double x1 = ((double)num) / ((double)pairs.size());
		double x2 = 1 - x1;
		
		x1 = x1 == 0 ? 0 : x1 * Math.log(x1);
		x2 = x2 == 0 ? 0 : x2 * Math.log(x2);
		
		return -1 * (x1 + x2);
	}

	private static class Pair {
		public boolean corresponding;
		public double value;
	}
}
