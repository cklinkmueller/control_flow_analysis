package bpm.matching.io.test;

import java.util.Collection;
import java.util.List;

import bpm.Configuration;
import bpm.io.ModelReader;
import bpm.model.Node;
import bpm.model.Process;
import bpm.model.ProcessUtilities;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class ModelReaderTest {
	public static void main(String[] args) {
		for (String dataset : Configuration.datasets) {
			System.out.println(dataset);
			
			ModelReader reader = Configuration.datasetModelReaders.get(dataset);
			List<Process> processes = reader.read(Configuration.datasetModelsFolder.get(dataset));
			System.out.println("\tmodels: " + processes.size());
			
			int min = Integer.MAX_VALUE;
			int max = 0;
			double ave = 0;
			for (Process process : processes) {
				Collection<Node> acs = ProcessUtilities.getActivities(process);
				min = Math.min(min, acs.size());
				max = Math.max(max, acs.size());
				ave += acs.size();
			}
			
			System.out.println("\tmin: " + min);
			System.out.println("\tmax: " + max);
			System.out.println("\tave: " + ave / processes.size());
			
			System.out.println();
		}
	}
}
