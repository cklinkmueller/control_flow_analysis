package bpm.matching.io.test;

import java.util.List;

import bpm.Configuration;
import bpm.io.AlignmentReader;
import bpm.io.ModelReader;
import bpm.model.Alignment;
import bpm.model.Process;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class AlignmentReaderTest {
	public static void main(String[] args) {
		for (String dataset : Configuration.datasets) {
			System.out.println(dataset);
			
			ModelReader reader = Configuration.datasetModelReaders.get(dataset);
			List<Process> processes = reader.read(Configuration.datasetModelsFolder.get(dataset));
		
			AlignmentReader alignmentReader = Configuration.datasetAlignmentReader.get(dataset);
			List<Alignment> alignments = alignmentReader.read(Configuration.datasetAlignmentsFolder.get(dataset), processes);
			
			System.out.println("\talignments: " + alignments.size());
			
			int min = Integer.MAX_VALUE;
			int max = 0;
			double ave = 0;
			
			for (Alignment al : alignments) {
				int cor = al.numberOfCorrespondences();
				min = Math.min(min, cor);
				max = Math.max(max, cor);
				ave += cor;
			}
			
			System.out.println("\tmin: " + min);
			System.out.println("\tmax: " + max);
			System.out.println("\tave: " + ave / alignments.size());
			System.out.println("\tcor: " + ave);
			
			System.out.println();
		}
	}
}
