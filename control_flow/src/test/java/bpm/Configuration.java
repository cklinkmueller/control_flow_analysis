package bpm;

import java.util.HashMap;
import java.util.LinkedList;

import bpm.io.AlignmentReader;
import bpm.io.ModelReader;
import bpm.io.XmlAlignmentReader;
import bpm.io.XmlModelReader;

/*
 * Copyright (c) 2012 - 2016 Christopher Klinkmüller
 * 
 * This software is released under the terms of the
 * MIT license. See http://opensource.org/licenses/MIT
 * for more information.
 */

public class Configuration {
	public static final LinkedList<String> datasets;
	public static final LinkedList<String> developmentDatasets;
	public static final HashMap<String, String> datasetModelsFolder;
	public static final HashMap<String, String> datasetAlignmentsFolder;
	public static final HashMap<String, ModelReader> datasetModelReaders;
	public static final HashMap<String, AlignmentReader> datasetAlignmentReader;
	public static final HashMap<String, String> datasetOutputFolder;
	
	static {
		datasets = new LinkedList<String>();
		developmentDatasets = new LinkedList<String>();
		datasetModelsFolder = new HashMap<String, String>();
		datasetAlignmentsFolder = new HashMap<String, String>();
		datasetModelReaders = new HashMap<String, ModelReader>();
		datasetAlignmentReader = new HashMap<String, AlignmentReader>();
		datasetOutputFolder = new HashMap<String, String>();
		
		// birth registration
		String set = "BR";
		Configuration.datasets.add(set);
		Configuration.developmentDatasets.add(set);
		Configuration.datasetOutputFolder.put(set, "./src/test/resources/birth registration");
		Configuration.datasetModelsFolder.put(set, "./src/test/resources/birth registration/model");
		Configuration.datasetAlignmentsFolder.put(set, "./src/test/resources/birth registration/gold standard");
		Configuration.datasetModelReaders.put(set, new XmlModelReader());
		Configuration.datasetAlignmentReader.put(set, new XmlAlignmentReader());
		
		// university admission
		set = "UA";
		Configuration.datasets.add(set);
		Configuration.developmentDatasets.add(set);
		Configuration.datasetOutputFolder.put(set, "./src/test/resources/university admission");
		Configuration.datasetModelsFolder.put(set, "./src/test/resources/university admission/model");
		Configuration.datasetAlignmentsFolder.put(set, "./src/test/resources/university admission/gold standard");
		Configuration.datasetModelReaders.put(set, new XmlModelReader());
		Configuration.datasetAlignmentReader.put(set, new XmlAlignmentReader());
		
		// asset management
		set = "AM";
		Configuration.datasets.add(set);
		Configuration.datasetOutputFolder.put(set, "./src/test/resources/asset management");
		Configuration.datasetModelsFolder.put(set, "./src/test/resources/asset management/model");
		Configuration.datasetAlignmentsFolder.put(set, "./src/test/resources/asset management/gold standard");
		Configuration.datasetModelReaders.put(set, new XmlModelReader());
		Configuration.datasetAlignmentReader.put(set, new XmlAlignmentReader());
		
		/*
		 * For each dataset you need to register the folders in which the models and the gold standards are located.
		 * You also need to specify and output folder to which the results of the control flow analyses are written.
		 * Finally, the readers to import the models as well as the gold standard need to be configured.
		 * For each of the datasets a configuration is provided below:
		 * 
		 * 
		 * // birth registration
		 * String set = "BR";
		 * datasets.add(set);
		 * developmentDatasets.add(set);
		 * datasetOutputFolder.put(set, "[...path_to_folder...]/birth");
		 * datasetModelsFolder.put(set, "[...path_to_folder...]/birth/model");
		 * datasetAlignmentsFolder.put(set, "[...path_to_folder...]/birth/gold standard");
		 * datasetModelReaders.put(set, new PetriNetModelReader());
		 * datasetAlignmentReader.put(set, new CsvAlignmentReader());
		 * 
		 * // university admission
		 * set = "UA";
		 * datasets.add(set);
		 * developmentDatasets.add(set);
		 * datasetOutputFolder.put(set, "[...path_to_folder...]/uni");
		 * datasetModelsFolder.put(set, "[...path_to_folder...]/uni/model");
		 * datasetAlignmentsFolder.put(set, "[...path_to_folder...]/uni/gold standard");
		 * datasetModelReaders.put(set, new PetriNetModelReader());
		 * datasetAlignmentReader.put(set, new CsvAlignmentReader());
		 * 
		 * // asset management
		 * set = "AM";
		 * datasets.add(set);
		 * datasetOutputFolder.put(set, "[...path_to_folder...]/sap");
		 * datasetModelsFolder.put(set, "[...path_to_folder...]/sap/model");
		 * datasetAlignmentsFolder.put(set, "[...path_to_folder...]/sap/gold standard");
		 * datasetModelReaders.put(set, new EpcModelReader());
		 * datasetAlignmentReader.put(set, new AMAlignmentReader());
		 */
	}
}
