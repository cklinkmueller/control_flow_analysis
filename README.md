This project provides the source code for the experiments, analyses, and matching techniques introduced in the article:
Klinkmüller, C. and Weber, I.: Analyzing Control Flow Information to Improve the Effectiveness of Process Model Matching Techniques, Decision Support Systems (2017), https://doi.org/10.1016/j.dss.2017.06.002.

Requirements:
	1. Install the Java SE Development Kit 8.
	2. Install the Eclipse IDE for Java EE Developers.
	3. Run Eclipse and import the project into your workspace via the context menu in the package explorer (Import -> Archive File)
	4. To run some of the control flow analyses you also need to install the statistics software package R.

Run the Code
	- The following programs for the control flow proposition and evaluations are located in the source folder "src/test/java".
		I. Data set characteristics:
			- bpm.matching.io.test.AlignmentReaderTest.java
			- bpm.matching.io.test.ModelReaderTest.java
		II. Control flow propositions:
			a. Compare 
				- bpm.matching.properties.InfoGainProgram.java (information gain computation for properties)
				- bpm.matching.properties.PropertyExport.java (csv-export of properties for Kolmogorov-Smirnov test)
			b. Clustering
				- bpm.matching.fragments.FragmentCountProgram.java (count of potential rpst fragments and (connected) sub-graphs
				- note that the corresponding clusters have been manually classified
			c. Consistency
				- bpm.matching.ors.GoldStandardOrsTest.java (computation of order relation scores for gold standard alignments)
				- bpm.matching.ors.RandomAlignmentTest.java (generation of random alignments for correlation of order relation score and effectiveness)
				- bpm.matching.algorithm.AMContestMatcherSelection.java (selection of matchers based on contest results for AM data set)
				- bpm.matching.algorithm.BRContestMatcherSelection.java (selection of matchers based on contest results for BR data set)
		III. Evaluation of OPBOT
				- bpm.matching.algorithm.EvaluationBOT.java (determination of BOT configuration with maximum f-measure)
				- bpm.matching.algorithm.EvaluationOPBOT.java (effectiveness and efficiency of OPBOT)
				- bpm.matching.algorithm.EvaluationManualConfiguration.java (effort and effectiveness of manual configuration)
	- Note that some R-scripts for the analysis of control flow propositions are provided in the folder "r_script".
		I. Compare
			- kstest.R (Kolmogorov-Smirnov test and box plot generation for control flow properties)
		II. Consistency
			- ors_correlation.R (spearman rank correlation between order relation score and effectiveness of random alignments)
	- The code for BOT, OPBOT and the control flow propositions is located in the folder "src/main/java". 
	- To run the code
		1. In eclipse run mvn-clean on the "pom.xml" (right click on pom.xml --> Run as --> mvn clean) to download all required software packages.
		2. Choose one of the programs in the source folder "src/test/java" and run it (right click on the class --> Run as --> Java Application)

Data:
	Development and Evaluation Data
		- The data sets are located in the folder "src/test/resources" in a self-defined xml format
		- In case you want to use the original formats, follow these steps
		1. Download the datasets from the following sources:
			BR: http://www.henrikleopold.com/downloads/ --> Birth Registration Data Set
			UA: http://www.henrikleopold.com/downloads/ --> University Admission Data Set --> BPMN Version (Process Model Matching Contest 2015)
			AM: https://ai.wu.ac.at/emisa2015/contest.php
		2. unpack each dataset into a separate folder and make sure that the files for the models and the gold standards are in non-nested sub-folders.
		3. register the datasets int the "bpm.Configuration" class in the source folder "src/test/java". (More instructions in the class file)
	Results of Contest Matchers	
		- the results of the matchers participating in the matching contest 2015 are located in the folder "src/test/resources"
		- they can be downloaded from https://ai.wu.ac.at/emisa2015/contest.php (link in the section "Participating Systems")
	
Contact:
	For any questions contact the first author of the above mentioned paper.

